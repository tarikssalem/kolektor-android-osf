# Change Log

## [kolektor](https://bitbucket.org/tarikssalem/funf-core-android/branch/kolektor) (2016-01-13)

**Enhancements:**

- Migrated from Ant to Gradle build system. [\#1](https://bitbucket.org/tarikssalem/funf-core-android/issues/1)
- Introducing JSON database file. [\#2](https://bitbucket.org/tarikssalem/funf-core-android/issues/2)

**Fixes:**
