/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.datasource;

import io.kolektor.android.osf.config.Configurable;
import io.kolektor.android.osf.probe.Probe;

public class ProbeDataSource extends StartableDataSource {

    @Configurable
    protected Probe source;

    ProbeDataSource() {
    }

    @Override
    protected void onStart() {
        source.registerListener(delegator);
    }

    @Override
    protected void onStop() {
        if (source instanceof Probe.StopableProbe) {
            ((Probe.StopableProbe) source).unregisterListener(delegator);
        }
    }

}
