/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.action;

import android.database.SQLException;
import android.util.Log;

import com.google.gson.JsonElement;

import java.math.BigDecimal;

import io.kolektor.android.osf.config.Configurable;
import io.kolektor.android.osf.config.RuntimeTypeAdapterFactory;
import io.kolektor.android.osf.json.IJsonObject;
import io.kolektor.android.osf.probe.Probe;
import io.kolektor.android.osf.probe.builtin.ProbeKeys;
import io.kolektor.android.osf.storage.DatabaseHelper;

import static io.kolektor.android.osf.util.LogUtil.TAG;

public class WriteAction extends Action implements Probe.DataListener {

    @Configurable
    private DatabaseHelper dbHelper = null;

    WriteAction() {
    }

    public WriteAction(DatabaseHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    protected void execute(String key, IJsonObject data) {
        if (key == null || data == null) {
            Log.w(TAG, "WriteAction: Unable to save probe data. Not all required values specified: " +
                    "key = [" + key + "], data = [" + data + "]");
            return;
        }

        final BigDecimal timestamp = data.get(ProbeKeys.BaseProbeKeys.TIMESTAMP).getAsBigDecimal();
        if (timestamp == null) {
            Log.w(TAG, "WriteAction: Unable to save probe data. Timestamp not specified: " +
                    "key = [" + key + "], data = [" + data + "]");
            throw new SQLException("Timestamp not specified.");
        }

        synchronized (dbHelper) {
            dbHelper.insert(key, timestamp, data);
        }
    }

    @Override
    public void onDataReceived(IJsonObject probeConfig, IJsonObject data) {
        final String key = extractType(probeConfig);
        final IJsonObject finalData = data;
        ensureHandlerExists();
        getHandler().post(new Runnable() {
            @Override
            public void run() {
                execute(key, finalData);
            }
        });
    }

    @Override
    public void onDataCompleted(IJsonObject probeConfig, JsonElement checkpoint) {
        String key = extractType(probeConfig);
        Log.d(TAG, "WriteAction: Finished writing probe data. key = [" + key + "]");
    }

    protected String extractType(IJsonObject probeConfig) {
        return probeConfig.get(RuntimeTypeAdapterFactory.TYPE).getAsString();
    }

    protected boolean isLongRunningAction() {
        return true;
    }

}
