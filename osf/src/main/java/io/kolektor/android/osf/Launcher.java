/*
 * Copyright (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import static io.kolektor.android.osf.util.LogUtil.TAG;

public class Launcher extends BroadcastReceiver {

    private static boolean launched = false;

    public static void launch(Context context) {
        Log.d(TAG, "Launcher: Launching...");
        Intent i = new Intent(context.getApplicationContext(), OsfService.class);
        context.getApplicationContext().startService(i);
        launched = true;
        Log.d(TAG, "Launcher: Launched!");
    }

    // TODO: This isn't fail proof. What if the service was stopped?
    public static boolean isLaunched() {
        return launched;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        launch(context);
    }

}
