/*
 * Copyright (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.UUID;

public class UuidUtil {

    public static final String INSTALLATION_UUID_KEY = "INSTALLATION_UUID";
    public static final String OSF_UTILS_PREFS = "io.kolektor.android.osf.Utils";
    public static String uuid = null;

    public static String getInstallationId(Context context) {
        if (UuidUtil.uuid == null) {
            SharedPreferences prefs = AsyncSharedPrefs.async(context.getSharedPreferences(
                    UuidUtil.OSF_UTILS_PREFS, Context.MODE_PRIVATE));
            UuidUtil.uuid = prefs.getString(UuidUtil.INSTALLATION_UUID_KEY, null);
            if (UuidUtil.uuid == null) {
                UuidUtil.uuid = UUID.randomUUID().toString();
                prefs.edit().putString(UuidUtil.INSTALLATION_UUID_KEY, UuidUtil.uuid).commit();
            }
        }
        return UuidUtil.uuid;
    }

}
