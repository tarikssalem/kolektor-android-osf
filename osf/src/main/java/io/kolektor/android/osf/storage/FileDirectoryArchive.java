/*
 * Copyright (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.storage;

import android.content.Context;

import java.io.File;
import java.io.FilenameFilter;
import java.security.PublicKey;

import javax.crypto.SecretKey;

import io.kolektor.android.osf.util.NameGenerator;

/**
 * A simple directory based archive for files. A file may be able to be archived more than once
 * depending on the name generator strategy that is used.
 */
public class FileDirectoryArchive implements FileArchive {

    private final File archiveDir;
    private final NameGenerator nameGenerator;
    private final FileCopier fileCopier;
    private final DirectoryCleaner cleaner;

    public FileDirectoryArchive(File archiveDir, NameGenerator nameGenerator, FileCopier fileCopier,
            DirectoryCleaner cleaner) {
        this.archiveDir = archiveDir;
        this.nameGenerator = nameGenerator;
        this.fileCopier = fileCopier;
        this.cleaner = cleaner;
        archiveDir.mkdirs();
    }

    public static FileDirectoryArchive getSimpleFileArchive(File archiveDir) {
        return new FileDirectoryArchive(archiveDir, new NameGenerator.IdentityNameGenerator(),
                new FileCopier.SimpleFileCopier(), new DirectoryCleaner.KeepAll());
    }

    public static FileDirectoryArchive getRollingFileArchive(File archiveDir) {
        return new FileDirectoryArchive(archiveDir, new NameGenerator.IdentityNameGenerator(),
                new FileCopier.SimpleFileCopier(), new DirectoryCleaner.KeepUnderPercentageOfDiskFree(0.5, 10000000));
    }

    /**
     * Creates an instance of an archive able compressing and encrypting files.
     * {@link NameGenerator.SystemUniqueTimestampNameGenerator} is used for generating a filename.
     *
     * @param archiveDir     archive directory
     * @param context        context
     * @param compress       <code>true</code> for compressing
     * @param key            if not <code>null</code>, files are encrypted using this secret key,
     *                       if <code>null</code>, the parameters <code>transformation</code> and
     *                       <code>provider</code> are ignored
     * @param transformation the name of the transformation, must be defined if parameter
     *                       <code>key</code> is not <code>null</code>
     * @param provider       the name of the provider, must be defined if parameter
     *                       <code>key</code> is not <code>null</code>
     * @return instance of {@link FileDirectoryArchive} with the given parameters
     */
    public static FileDirectoryArchive getEncryptedFileArchive(File archiveDir, Context context,
            Boolean compress, SecretKey key, String transformation, String provider) {
        FileCopier copier;
        if (compress) {
            copier = (key == null) ? new FileCopier.CompressedFileCopier() :
                    new FileCopier.CompressedEncryptedFileCopier(key, transformation, provider);
        } else {
            copier = (key == null) ? new FileCopier.SimpleFileCopier() :
                    new FileCopier.EncryptedFileCopier(key, transformation, provider);
        }
        String suffix = copier.getRecommendedFilenameExtension();
        NameGenerator nameGenerator = new NameGenerator.CompositeNameGenerator(
                new NameGenerator.SystemUniqueTimestampNameGenerator(context),
                new NameGenerator.RequiredSuffixNameGenerator(suffix)
        );
        return new FileDirectoryArchive(archiveDir, nameGenerator, copier, new DirectoryCleaner.KeepAll());
    }

    /**
     * See {@link FileCopier.GzipRsaAesFileCopier}.
     *
     * @param archiveDir     archive directory
     * @param context        context
     * @param key            if not <code>null</code>, files are encrypted using this secret key,
     *                       if <code>null</code>, the parameters <code>transformation</code> and
     *                       <code>provider</code> are ignored
     * @param provider       the name of the provider, must be defined if parameter
     *                       <code>key</code> is not <code>null</code>
     * @return instance of {@link FileDirectoryArchive} with the given parameters
     */
    public static FileDirectoryArchive createGzipRsaAesFileArchive(File archiveDir, Context context,
            PublicKey key, String provider) {
        FileCopier copier = new FileCopier.GzipRsaAesFileCopier(key, provider);
        String suffix = copier.getRecommendedFilenameExtension();
        NameGenerator nameGenerator = new NameGenerator.CompositeNameGenerator(
                new NameGenerator.SystemUniqueTimestampNameGenerator(context),
                new NameGenerator.RequiredSuffixNameGenerator(suffix)
        );
        return new FileDirectoryArchive(archiveDir, nameGenerator, copier, new DirectoryCleaner.KeepAll());
    }

    @Override
    public boolean add(File item) {
        this.archiveDir.mkdirs();
        File archiveFile = new File(archiveDir, nameGenerator.generateName(item.getName()));
        boolean result = fileCopier.copy(item, archiveFile);
        cleaner.clean(archiveDir);
        return result;
    }

    @Override
    public File[] getAll() {
        cleaner.clean(archiveDir);
        File[] files = archiveDir.listFiles();
        return (files == null) ? new File[0] : files;
    }

    @Override
    public boolean remove(File item) {
        if (contains(item)) {
            return item.delete();
        }
        return false;
    }

    @Override
    public boolean contains(final File item) {
        final String itemFilename = item.getName();
        String[] files = archiveDir.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return itemFilename.equals(filename);
            }
        });
        return files != null && files.length > 0;
    }

}
