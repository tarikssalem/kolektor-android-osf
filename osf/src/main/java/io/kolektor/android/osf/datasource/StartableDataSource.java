/*
 * Copyright (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.datasource;

import com.google.gson.JsonElement;

import io.kolektor.android.osf.json.IJsonObject;
import io.kolektor.android.osf.probe.Probe;

public class StartableDataSource implements Startable, DataSource {

//    public static enum State {
//        OFF,
//        ON
//    };
//
//    protected State currentState = State.OFF;

    protected Probe.DataListener outputListener;

    protected Probe.DataListener delegator = new Probe.DataListener() {

        @Override
        public void onDataReceived(IJsonObject probeConfig, IJsonObject data) {
            if (outputListener != null) {
                outputListener.onDataReceived(probeConfig, data);
            }
        }

        @Override
        public void onDataCompleted(IJsonObject probeConfig,
                                    JsonElement checkpoint) {
            if (outputListener != null) {
                outputListener.onDataCompleted(probeConfig, checkpoint);
            }
        }
    };

    public Probe.DataListener getDelegator() {
        return delegator;
    }

    @Override
    public final void start() {
//        if (currentState == State.ON)
//            return;
//        currentState = State.ON;
        onStart();
    }

    @Override
    public final void stop() {
//        if (currentState == State.OFF)
//            return;
//        currentState = State.OFF;
        onStop();
    }

    @Override
    public void setListener(Probe.DataListener listener) {
        this.outputListener = listener;
    }

    protected void onStart() {

    }

    protected void onStop() {

    }
}
