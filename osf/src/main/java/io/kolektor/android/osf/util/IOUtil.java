/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.util;

import android.net.Uri;
import android.util.Log;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.BasicResponseHandler;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.params.HttpParams;

import static io.kolektor.android.osf.util.LogUtil.TAG;

public class IOUtil {

    public static String inputStreamToString(InputStream is, String encoding) throws IOException {
        final char[] buffer = new char[0x10000];
        StringBuilder out = new StringBuilder();
        Reader in = new InputStreamReader(is, encoding);
        int read;
        do {
            read = in.read(buffer, 0, buffer.length);
            if (read > 0) {
                out.append(buffer, 0, read);
            }
        } while (read >= 0);
        return out.toString();
    }

    public static String httpGet(String uri, HttpParams params) {
        String responseBody = null;
        HttpClient httpclient = new DefaultHttpClient();
        StringBuilder uriBuilder = new StringBuilder(uri);
        HttpGet httpget = new HttpGet(uriBuilder.toString());
        if (params != null) {
            httpget.setParams(params);
        }
        ResponseHandler<String> responseHandler = new BasicResponseHandler();
        try {
            responseBody = httpclient.execute(httpget, responseHandler);
        } catch (ClientProtocolException e) {
            Log.e(TAG, "IOUtil: HttpGet Error", e);
            responseBody = null;
        } catch (IOException e) {
            Log.e(TAG, "IOUtil: HttpGet Error", e);
            responseBody = null;
        } finally {
            httpclient.getConnectionManager().shutdown();
        }
        return responseBody;
    }

    /**
     * Closes a stream and swallows null cases or IOExceptions.
     *
     * @param stream
     */
    public static boolean close(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
                return true;
            } catch (IOException e) {
                Log.e(TAG, "IOUtil: Error closing stream", e);
            }
        }
        return false;
    }

    public static boolean isValidUrl(String url) {
        boolean isValidUrl = false;
        if (url != null && !url.trim().equals("")) {
            try {
                Uri test = Uri.parse(url);
                isValidUrl = test.getScheme() != null
                        && test.getScheme().startsWith("http")
                        && test.getHost() != null
                        && !test.getHost().trim().equals("");
            } catch (Exception e) {
                Log.w(TAG, "IOUtil: URL not valid.");
            }
        }
        return isValidUrl;
    }

}
