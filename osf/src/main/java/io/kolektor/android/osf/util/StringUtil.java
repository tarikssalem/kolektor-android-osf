/*
 * Copyright (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.util;

import java.util.Collection;
import java.util.Iterator;

public class StringUtil {

    private StringUtil() {

    }

    /**
     * Convenience function for joining strings using a delimeter.
     *
     * @param strings   collection of strings to be joined
     * @param delimeter delimiting string
     * @return the resulting string
     */
    public static String join(final Collection<?> strings, String delimeter) {
        if (delimeter == null) {
            delimeter = ",";
        }
        if (strings.isEmpty()) {
            return "";
        }
        StringBuffer joined = new StringBuffer();
        Iterator<?> stringIter = strings.iterator();
        joined.append(stringIter.next().toString());
        while (stringIter.hasNext()) {
            joined.append(delimeter);
            joined.append(stringIter.next().toString());
        }
        return joined.toString();
    }

    /**
     * Intended for generating safe filenames.
     *
     * @param s the string to be sanitized
     * @return the resulting sanitized string
     */
    public static String sanitize(String s) {
        return s == null ? "" : s.replaceAll("[^a-zA-Z0-9-_\\.]", "_").toLowerCase();
    }

}
