/**
 * 
 * Funf: Open Sensing Framework
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland.
 * Acknowledgments: Alan Gardner
 * Contact: nadav@media.mit.edu
 * 
 * This file is part of Funf.
 * 
 * Funf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * Funf is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with Funf. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package io.kolektor.android.osf.probe.builtin;

import java.util.HashMap;
import java.util.Map;

import android.net.Uri;
import io.kolektor.android.osf.Schedule;
import io.kolektor.android.osf.probe.Probe;

/**
 * @author alangardner
 *
 */
@Schedule.DefaultSchedule(interval=36000)
@Probe.DisplayName("SMS Log Probe")
@Probe.RequiredPermissions(android.Manifest.permission.READ_SMS)
public class SmsProbe extends DatedContentProviderProbe implements ProbeKeys.SmsKeys {

	@Override
	protected Uri getContentProviderUri() {
		return ProbeKeys.AndroidInternal.Sms.CONTENT_URI;
	}

	@Override
	protected String getDateColumnName() {
		return ProbeKeys.AndroidInternal.Sms.DATE;
	}

	@Override
	protected Map<String, CursorCell<?>> getProjectionMap() {
		Map<String, CursorCell<?>> projectionMap = new HashMap<String, CursorCell<?>>();
		projectionMap.put(ProbeKeys.AndroidInternal.Sms.TYPE, intCell());
		projectionMap.put(ProbeKeys.AndroidInternal.Sms.THREAD_ID, intCell());
		projectionMap.put(ProbeKeys.AndroidInternal.Sms.ADDRESS, sensitiveStringCell()); // TODO: figure out if we have to normalize this first (maybe phone number)
		projectionMap.put(ProbeKeys.AndroidInternal.Sms.PERSON_ID, longCell());
		projectionMap.put(ProbeKeys.AndroidInternal.Sms.DATE, longCell());
		projectionMap.put(ProbeKeys.AndroidInternal.Sms.READ, booleanCell());
		//projectionMap.put(Sms.SEEN, booleanCell()); //Not Supported on all devices
		projectionMap.put(ProbeKeys.AndroidInternal.Sms.STATUS, intCell());
		projectionMap.put(ProbeKeys.AndroidInternal.Sms.SUBJECT, sensitiveStringCell());
		projectionMap.put(ProbeKeys.AndroidInternal.Sms.BODY, sensitiveStringCell());
		projectionMap.put(ProbeKeys.AndroidInternal.Sms.PERSON, sensitiveStringCell());
		projectionMap.put(ProbeKeys.AndroidInternal.Sms.PROTOCOL, intCell());
		projectionMap.put(ProbeKeys.AndroidInternal.Sms.REPLY_PATH_PRESENT, booleanCell());
		projectionMap.put(ProbeKeys.AndroidInternal.Sms.SERVICE_CENTER, stringCell());
		projectionMap.put(ProbeKeys.AndroidInternal.Sms.LOCKED, booleanCell());
		//projectionMap.put(Sms.ERROR_CODE, intCell());  //Not Supported on all devices
		//projectionMap.put(Sms.META_DATA, hashedStringCell());  Doesn't exist for some reason
		return projectionMap;
	}

}
