/*
 * Copyright (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.pipeline;

import android.content.Context;
import android.os.Handler;

import com.google.gson.JsonElement;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.kolektor.android.osf.OsfService;
import io.kolektor.android.osf.action.ActionAdapter;
import io.kolektor.android.osf.action.ArchiveAction;
import io.kolektor.android.osf.action.UpdateAction;
import io.kolektor.android.osf.action.UploadAction;
import io.kolektor.android.osf.action.WriteAction;
import io.kolektor.android.osf.concurrent.LooperThread;
import io.kolektor.android.osf.config.ConfigUpdater;
import io.kolektor.android.osf.config.Configurable;
import io.kolektor.android.osf.datasource.StartableDataSource;
import io.kolektor.android.osf.json.IJsonObject;
import io.kolektor.android.osf.json.JsonUtils;
import io.kolektor.android.osf.probe.Probe;
import io.kolektor.android.osf.storage.DatabaseHelper;
import io.kolektor.android.osf.storage.DefaultArchive;
import io.kolektor.android.osf.storage.FileArchive;
import io.kolektor.android.osf.storage.FileUploader;
import io.kolektor.android.osf.storage.JsonDatabaseHelper;
import io.kolektor.android.osf.storage.RemoteFileArchive;
import io.kolektor.android.osf.storage.SqliteDatabaseHelper;
import io.kolektor.android.osf.util.StringUtil;

public class BasicPipeline implements Pipeline, Probe.DataListener {

    public static final String ACTION_ARCHIVE = "archive";
    public static final String ACTION_UPLOAD = "upload";
    public static final String ACTION_UPDATE = "update";
    public static final String CONFIG_KEY_ARCHIVE = "archive";
    public static final String CONFIG_KEY_UPLOAD = "upload";
    public static final String CONFIG_KEY_UPDATE = "update";

    @Configurable(name = "name")
    protected String mName = "default";

    @Configurable(name = "version")
    protected int mVersion = 1;

    @Configurable(name = "database")
    protected String mDatabase = "json";

    @Configurable(name = "archive")
    protected FileArchive mArchive = null;

    @Configurable(name = "upload")
    protected RemoteFileArchive mUpload = null;

    @Configurable(name = "update")
    protected ConfigUpdater mUpdate = null;

    @Configurable(name = "data")
    protected List<StartableDataSource> mData = null;

    @Configurable(name = "schedules")
    protected Map<String, StartableDataSource> mSchedules = null;

    private boolean mEnabled;
    private OsfService mOsfService;
    private LooperThread mThread;
    private Handler mHandler;
    private FileUploader mUploader;
    private DatabaseHelper mDbHelper;
    private WriteAction mWriteAction;
    private ArchiveAction mArchiveAction;
    private UploadAction mUploadAction;
    private UpdateAction mUpdateAction;
    private Set<Probe.DataListener> mDataListeners = new HashSet<>();

    @Override
    public void onCreate(OsfService osfService) {
        mOsfService = osfService;

        if (mArchive == null) {
            mArchive = new DefaultArchive(mOsfService, mName);
        }
        if (mUploader == null) {
            mUploader = new FileUploader(mOsfService);
            mUploader.start();
        }

        mDbHelper = getdDbHelperInstance(mOsfService);

        mThread = new LooperThread(getClass().getName());
        mThread.start();
        mHandler = mThread.getHandler();

        mWriteAction = new WriteAction(mDbHelper);
        mWriteAction.setHandler(mHandler);
        mArchiveAction = new ArchiveAction(mArchive, mDbHelper);
        mArchiveAction.setHandler(mHandler);
        mUploadAction = new UploadAction(mArchive, mUpload, mUploader);
        mUploadAction.setHandler(mHandler);
        mUpdateAction = new UpdateAction(mName, mOsfService, mUpdate);
        mUpdateAction.setHandler(mHandler);

        mThread.post(new Runnable() {
            @Override
            public void run() {
                startDataSources();
            }
        });
    }

    @Override
    public void onRun(String action, JsonElement config) {
        if (ACTION_ARCHIVE.equals(action)) {
            mArchiveAction.run();
        } else if (ACTION_UPLOAD.equals(action)) {
            mUploadAction.run();
        } else if (ACTION_UPDATE.equals(action)) {
            mUpdateAction.run();
        }
    }

    @Override
    public void onDestroy() {
        if (mUploader != null) {
            mUploader.quit();
        }
        mThread.post(new Runnable() {
            @Override
            public void run() {
                stopDataSources();
            }
        });
        mThread.quitSafely();
    }

    @Override
    public void onDataReceived(IJsonObject probeConfig, IJsonObject data) {
        mWriteAction.onDataReceived(probeConfig, data);
        for (Probe.DataListener dataListener : mDataListeners) {
            dataListener.onDataReceived(probeConfig, data);
        }
    }

    @Override
    public void onDataCompleted(IJsonObject probeConfig, JsonElement checkpoint) {
        mWriteAction.onDataCompleted(probeConfig, checkpoint);
        for (Probe.DataListener dataListener : mDataListeners) {
            dataListener.onDataCompleted(probeConfig, checkpoint);
        }
    }

    @Override
    public boolean isEnabled() {
        return mEnabled;
    }

    protected DatabaseHelper getdDbHelperInstance(Context context) {
        switch (mDatabase) {
            case "sqlite":
                return new SqliteDatabaseHelper(context, StringUtil.sanitize(mName), mVersion);
            case "json":
            default:
                return new JsonDatabaseHelper(context, StringUtil.sanitize(mName));
        }
    }

    protected void startDataSources() {
        if (!mEnabled) {
            for (StartableDataSource dataSource : mData) {
                dataSource.setListener(this);
            }

            if (mSchedules != null) {
                if (mSchedules.containsKey(CONFIG_KEY_ARCHIVE)) {
                    Probe.DataListener archiveListener = new ActionAdapter(mArchiveAction);
                    mSchedules.get(CONFIG_KEY_ARCHIVE).setListener(archiveListener);
                    mSchedules.get(CONFIG_KEY_ARCHIVE).start();
                }

                if (mSchedules.containsKey(CONFIG_KEY_UPLOAD)) {
                    Probe.DataListener uploadListener = new ActionAdapter(mUploadAction);
                    mSchedules.get(CONFIG_KEY_UPLOAD).setListener(uploadListener);
                    mSchedules.get(CONFIG_KEY_UPLOAD).start();
                }

                if (mSchedules.containsKey(CONFIG_KEY_UPDATE)) {
                    Probe.DataListener updateListener = new ActionAdapter(mUpdateAction);
                    mSchedules.get(CONFIG_KEY_UPDATE).setListener(updateListener);
                    mSchedules.get(CONFIG_KEY_UPDATE).start();
                }
            }

            for (StartableDataSource dataSource : mData) {
                dataSource.start();
            }

            mEnabled = true;
        }
    }

    private void stopDataSources() {
        if (mEnabled) {
            for (StartableDataSource dataSource : mData) {
                dataSource.stop();
            }
            mEnabled = false;
        }
    }

    public OsfService getOsfService() {
        return mOsfService;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public int getVersion() {
        return mVersion;
    }

    public void setVersion(int version) {
        this.mVersion = version;
    }

    public FileArchive getArchive() {
        return mArchive;
    }

    public void setArchive(FileArchive archive) {
        mArchive = archive;
    }

    public RemoteFileArchive getUpload() {
        return mUpload;
    }

    public void setUpload(RemoteFileArchive upload) {
        mUpload = upload;
    }

    public ConfigUpdater getUpdate() {
        return mUpdate;
    }

    public void setUpdate(ConfigUpdater update) {
        mUpdate = update;
    }

    public void addListener(Probe.DataListener... listeners) {
        if (listeners != null) {
            for (Probe.DataListener listener : listeners) {
                mDataListeners.add(listener);
            }
        }
    }

    public void removeListener(Probe.DataListener... listeners) {
        if (listeners != null) {
            for (Probe.DataListener listener : listeners) {
                mDataListeners.remove(listener);
            }
        }
    }

    public void removeAllListeners() {
        mDataListeners.clear();
    }

    public IJsonObject getImmutableProbeConfig(String probeConfig) {
        Probe probe = getOsfService().getGson().fromJson(probeConfig, Probe.class);
        IJsonObject probeJson = (IJsonObject) JsonUtils.immutable(
                getOsfService().getGson().toJsonTree(probe));
        return probeJson;
    }

}
