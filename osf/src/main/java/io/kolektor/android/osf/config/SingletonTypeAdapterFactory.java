/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.config;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.internal.Streams;
import com.google.gson.internal.bind.JsonTreeReader;
import com.google.gson.internal.bind.JsonTreeWriter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.kolektor.android.osf.json.JsonUtils;

/**
 * Same configuration should return the same object. A cache of each object created is kept
 * to ensure this.
 */
public class SingletonTypeAdapterFactory implements TypeAdapterFactory {

    private RuntimeTypeAdapterFactory delegate;
    private Map<String, Object> cache;

    public SingletonTypeAdapterFactory(RuntimeTypeAdapterFactory delegate) {
        this.delegate = delegate;
        this.cache = new HashMap<String, Object>();
    }

    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
        TypeAdapter<T> adapter = delegate.create(gson, type);
        return adapter == null ? null : new SingletonTypeAdapter<T>(adapter, type);
    }

    public Collection<Object> getCached() {
        return this.cache.values();
    }

    public void clearCache(Object o) {
        synchronized (cache) {
            Set<String> toRemove = new HashSet<String>();
            for (Map.Entry<String, Object> entry : cache.entrySet()) {
                if (o == entry.getValue()) {
                    toRemove.add(entry.getKey());
                }
            }
            for (String key : toRemove) {
                cache.remove(key);
            }
        }
    }

    public void clearCache() {
        synchronized (cache) {
            cache.clear();
        }
    }

    public class SingletonTypeAdapter<E> extends TypeAdapter<E> {

        private TypeAdapter<E> typeAdapter;
        private TypeToken<E> type;

        public SingletonTypeAdapter(TypeAdapter<E> typeAdapter, TypeToken<E> type) {
            this.typeAdapter = typeAdapter;
            this.type = type;
        }

        @Override
        public void write(JsonWriter out, E value) throws IOException {
            typeAdapter.write(out, value);
        }

        /**
         * Returns an object of a given type and configuration. It creates and keeps only
         * a single instance of an object with the given type and configuration.
         *
         * @param in JSON stream
         * @return deserialized object
         * @throws IOException
         */
        @Override
        public E read(JsonReader in) throws IOException {
            JsonElement el = Streams.parse(in);
            Class<? extends E> runtimeType = delegate.getRuntimeType(el, type);
            String configString = toImmutableJson(el);

            /*
             * The JSON input for serialization may not be identical with the JSON resulted
             * from deserialization of the very same object even though the configuration
             * is identical. It is because the deserialization process is adding also parameters
             * with default values which has not been present in the original JSON.
             *
             * That can be problematic when mapping JSON to object. That is why we do maybe
             * unnecessary deserialization and serialization to generate consistent JSON.
             *
             * TODO: A better idea to solve this problem?
             */
            E newObject;
            E cachedObject = runtimeType.cast(cache.get(configString));
            if (cachedObject != null) {
                return cachedObject;
            } else {
                newObject = typeAdapter.read(new JsonTreeReader(el));
                configString = toImmutableJson(newObject);
                cachedObject = runtimeType.cast(cache.get(configString));
            }
            if (cachedObject == null) {
                cache.put(configString, newObject);
                return newObject;
            }
            return cachedObject;
        }

        private String toImmutableJson(JsonElement el) {
            return JsonUtils.immutable(el).toString();
        }

        private String toImmutableJson(E object) throws IOException {
            return JsonUtils.immutable(toJsonTree(object)).toString();
        }

        private JsonElement toJsonTree(E object) throws IOException {
            JsonTreeWriter writer = new JsonTreeWriter();
            writer.setSerializeNulls(false);
            write(writer, object);
            return writer.get();
        }

        public void clearCache() {
            cache.clear();
        }
    }

}
