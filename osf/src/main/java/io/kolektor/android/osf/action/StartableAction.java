/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.action;

import android.util.Log;

import io.kolektor.android.osf.config.Configurable;
import io.kolektor.android.osf.datasource.Startable;
import io.kolektor.android.osf.datasource.Startable.TriggerAction;
import io.kolektor.android.osf.time.TimeUtil;

import static io.kolektor.android.osf.util.LogUtil.TAG;

public class StartableAction extends Action implements TriggerAction {

    @Configurable
    private Double duration = null;

    @Configurable
    private Startable target = null;

    StartableAction() {
    }

    public void setTarget(Startable target) {
        this.target = target;
    }

    protected void execute() {
        if (target == null)
            return;
        Log.d(TAG, "StartableAction: Running action start...");
        target.start();
        if (duration != null && duration > 0.0) {
            getHandler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "StartableAction: Running action stop...");
                    target.stop();
                }
            }, TimeUtil.secondsToMillis(duration));
        }
    }

    protected boolean isLongRunningAction() {
        return true;
    }

}
