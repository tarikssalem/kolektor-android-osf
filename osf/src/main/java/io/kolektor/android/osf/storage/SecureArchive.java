/*
 * Copyright (C) 2017 Tárik S. Salem.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.storage;

import android.util.Log;

import java.io.File;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

import io.kolektor.android.osf.config.Configurable;
import io.kolektor.android.osf.security.Base64Coder;

import static io.kolektor.android.osf.util.LogUtil.TAG;

/**
 * Provides a similar functionality as {@link DefaultArchive} but utilizing an asymmetric key
 * encryption scheme.
 * <p>
 * Data are compressed, encrypted and saved as files primarily located in an external storage
 * (usually an SD card) or in an internal storage if the external storage is not available.
 * It also maintains a rolling backup archive if the external storage is available.
 * <p>
 * The copied file contains an RSA encrypted random AES key followed by Gzip compressed and
 * AES encrypted data. The encrypted AES key makes the first 256 bytes of the generated files and
 * the rest are AES encrypted compressed data.
 * <p>
 * Archives are singletons by database name.
 */
public class SecureArchive extends AbstractArchive {

    private static final String PROVIDER = "BC";
    private static final String RSA_ALGORITHM = "RSA";

    /**
     * Base64 encoded RSA public key.
     */
    @Configurable
    protected String key;

    private PublicKey publicKey;
    private FileArchive delegateArchive;

    @Override
    protected FileArchive getDelegateArchive() {
        if (delegateArchive == null) {
            synchronized (this) {
                if (delegateArchive == null) {
                    try {
                        PublicKey key = getPublicKey();
                        File internalArchiveDir = getInternalArchiveDir();
                        File externalArchiveDir = getExternalArchiveDir();
                        File backupArchiveDir = getBackupArchiveDir();
                        FileArchive mainArchive = new CompositeFileArchive(
                                FileDirectoryArchive.createGzipRsaAesFileArchive(externalArchiveDir,
                                        context, key, PROVIDER),
                                FileDirectoryArchive.createGzipRsaAesFileArchive(internalArchiveDir,
                                        context, key, PROVIDER)
                        );
                        FileArchive backupArchive = FileDirectoryArchive.getRollingFileArchive(backupArchiveDir);
                        delegateArchive = new BackedUpArchive(mainArchive, backupArchive);
                    } catch (GeneralSecurityException e) {
                        Log.e(TAG, "SecureArchive: Archiving failed.", e);
                    }
                }
            }
        }
        return delegateArchive;
    }

    protected PublicKey getPublicKey() throws GeneralSecurityException {
        if (publicKey == null) {
            if (key != null) {
                publicKey = decodePublicKey(key);
            } else {
                throw new GeneralSecurityException("No public key defined.");
            }
        }
        return publicKey;
    }

    private static PublicKey decodePublicKey(String key) throws GeneralSecurityException {
        byte[] keyBytes = Base64Coder.decode(key);
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance(RSA_ALGORITHM);
        return kf.generatePublic(spec);
    }

}
