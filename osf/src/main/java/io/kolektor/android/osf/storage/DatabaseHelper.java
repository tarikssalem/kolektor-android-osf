package io.kolektor.android.osf.storage;

import java.io.File;
import java.math.BigDecimal;

import io.kolektor.android.osf.json.IJsonObject;

/**
 * Interface for a database.
 */
public interface DatabaseHelper {

    public final static String COLUMN_NAME = "name";
    public final static String COLUMN_TIMESTAMP = "timestamp";
    public final static String COLUMN_VALUE = "value";

    /**
     * Called to initialize a database.
     */
    public void initialize();

    /**
     * Inserts data to a database.
     *
     * @param key identifies the source (probe)
     * @param timestamp timestamp in seconds with milliseconds precision
     * @param data data
     */
    public void insert(String key, BigDecimal timestamp, IJsonObject data);

    /**
     * Called to close the database and release the reference.
     */
    public void close();

    /**
     * Deletes the database.
     */
    public boolean delete();

    /**
     * Returns the path of the currently used database.
     *
     * @return path to a storage file
     */
    public String getPath();

    /**
     * Returns a database file.
     *
     * @return file
     */
    public File getFile();

    /**
     * Returns <code>true</code> if the database is empty.
     *
     * @return <code>true</code> if empty, otherwise <code>false</code>
     */
    public boolean isEmpty();

    /**
     * Returns number of records.
     *
     * @return number of stored records
     */
    public int size();

}
