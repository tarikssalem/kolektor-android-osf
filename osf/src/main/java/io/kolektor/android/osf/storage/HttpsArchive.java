/*
 * Copyright (C) 2017 Tárik S. Salem.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.storage;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

import io.kolektor.android.osf.Schedule;
import io.kolektor.android.osf.config.Configurable;
import io.kolektor.android.osf.util.IOUtil;
import okhttp3.ConnectionSpec;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static io.kolektor.android.osf.util.LogUtil.TAG;

/**
 * Archives a file to a remote server through HTTP POST method.
 */
@Schedule.DefaultSchedule(interval = 21600)
public class HttpsArchive implements RemoteFileArchive {

    @Configurable(name = "https")
    private boolean mHttps = true;

    @Configurable(name = "url")
    private String mUrl;

    @Configurable(name = "wifiOnly")
    private boolean mWifiOnly = false;

    protected Context context;
    private OkHttpClient mHttpClient;

    public HttpsArchive() {
    }

    private void ensureClientExists() {
        if (mHttpClient == null) {
            if (mHttps) {
                mHttpClient = new OkHttpClient.Builder()
                        .connectionSpecs(Collections.singletonList(ConnectionSpec.MODERN_TLS))
                        .build();
            } else {
                mHttpClient = new OkHttpClient();
            }
        }
    }

    public static boolean uploadFile(OkHttpClient client, String url, File file) {
        RequestBody body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", file.getName(), RequestBody.create(null, file))
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        boolean success = false;
        try {
            Response response = client.newCall(request).execute();
            if (request != null && response.isSuccessful()) {
                success = true;
            } else {
                Log.e(TAG, "HttpsArchive: Something went wrong. response = " + response);
            }
        } catch (IOException e) {
            Log.e(TAG, "HttpsArchive: Upload error.", e);
        }
        return success;
    }

    public boolean add(File file) {
        ensureClientExists();
        return IOUtil.isValidUrl(mUrl) ? uploadFile(mHttpClient, mUrl, file) : false;
    }

    public boolean isAvailable() {
        if (context == null) {
            throw new IllegalArgumentException("Missing context.");
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        if (!mWifiOnly && netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else if (mWifiOnly) {
            State wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();
            if (State.CONNECTED.equals(wifiInfo) || State.CONNECTING.equals(wifiInfo)) {
                return true;
            }
        }
        return false;
    }

    public String getId() {
        return mUrl;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

}
