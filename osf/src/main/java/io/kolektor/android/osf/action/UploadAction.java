/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.action;

import android.util.Log;

import io.kolektor.android.osf.config.Configurable;
import io.kolektor.android.osf.storage.FileArchive;
import io.kolektor.android.osf.storage.FileUploader;
import io.kolektor.android.osf.storage.RemoteFileArchive;

import static io.kolektor.android.osf.util.LogUtil.TAG;

/**
 * This action is responsible for uploading the locally archived files to a remote archive.
 * It utilizes an "uploader" instance ({@link FileUploader} which implements the upload logic,
 * including the retry policy.
 */
public class UploadAction extends Action {

    @Configurable(name = "archive") // Matching the JSON pipeline configuration keys.
    private FileArchive localArchive = null;

    @Configurable(name = "upload") // Matching the JSON pipeline configuration keys.
    private RemoteFileArchive remoteArchive = null;

    @Configurable(name = "uploader")
    private FileUploader uploader;

    UploadAction() {
    }

    public UploadAction(FileArchive localArchive, RemoteFileArchive remoteArchive,
            FileUploader uploader) {
        this.localArchive = localArchive;
        this.remoteArchive = remoteArchive;
        this.uploader = uploader;
    }

    @Override
    protected void execute() {
        if (localArchive != null && remoteArchive != null && uploader != null) {
            uploader.run(localArchive, remoteArchive);
        } else {
            Log.w(TAG, "UploadAction: Failed!");
        }
    }

    @Override
    protected boolean isLongRunningAction() {
        return true;
    }

}