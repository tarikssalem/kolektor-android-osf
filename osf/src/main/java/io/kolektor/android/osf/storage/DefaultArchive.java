/*
 * Copyright (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.storage;

import android.content.Context;

import java.io.File;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import io.kolektor.android.osf.Schedule.DefaultSchedule;
import io.kolektor.android.osf.config.Configurable;
import io.kolektor.android.osf.security.Base64Coder;

/**
 * A default implementation of a file archive, which should be good enough for most cases.
 * It primarily archives the data to an external storage (usually an SD card) or to internal storage
 * if the external storage is not available. It also maintains a rolling backup archive
 * if the external storage is available.
 * <p>
 * This archive provides file compression and encryption as well.
 * <p>
 * Archives are singletons by database name.
 */
@DefaultSchedule(interval = 3600) // TODO: Don't understand what is it for here.
public class DefaultArchive extends AbstractArchive {

    private static final byte[] SALT = {
            (byte) 0xa6, (byte) 0xab, (byte) 0x09, (byte) 0x93,
            (byte) 0xf4, (byte) 0xcc, (byte) 0xee, (byte) 0x10
    };
    private static final int ITERATION_COUNT = 135;
    private static final int KEY_LENGTH = 128;
//    private static final String PBKD_ALGORITHM = "PBKDF2WithHmacSHA512";
    private static final String PBKD_ALGORITHM = "PBKDF2WithHmacSHA1";
    private static final String AES_ALGORITHM = "AES";
    protected static final String TRANSFORMATION = "AES/ECB/PKCS7Padding";
    protected static final String PROVIDER = "BC";

    @Configurable
    protected Boolean compress = false;

    @Configurable
    protected String key;

    @Configurable
    protected String password;

    private SecretKey secretKey = null;
    private FileArchive delegateArchive;

    public DefaultArchive() {

    }

    public DefaultArchive(Context ctx, String name) {
        this.context = ctx;
        this.name = name;
    }

    @Override
    protected FileArchive getDelegateArchive() {
        if (delegateArchive == null) {
            synchronized (this) {
                if (delegateArchive == null) {
                    SecretKey key = getSecretKey();
                    File internalArchiveDir = getInternalArchiveDir();
                    File externalArchiveDir = getExternalArchiveDir();
                    File backupArchiveDir = getBackupArchiveDir();
                    FileArchive mainArchive = new CompositeFileArchive(
                            FileDirectoryArchive.getEncryptedFileArchive(externalArchiveDir, context,
                                    compress, key, TRANSFORMATION, PROVIDER),
                            FileDirectoryArchive.getEncryptedFileArchive(internalArchiveDir, context,
                                    compress, key, TRANSFORMATION, PROVIDER)
                    );
                    FileArchive backupArchive = FileDirectoryArchive.getRollingFileArchive(backupArchiveDir);
                    delegateArchive = new BackedUpArchive(mainArchive, backupArchive);
                }
            }
        }
        return delegateArchive;
    }

    protected FileArchive resetDelegateArchive() {
        delegateArchive = null;
        return getDelegateArchive();
    }

    /**
     * Set the encryption key using a password. Does not store the password, but instead uses it
     * to derive a {@value PBKD_ALGORITHM} key to encrypt files.
     *
     * @param password uses char[] instead of String to prevent caching
     */
    public void setEncryptionPassword(char[] password) {
        if (password == null || password.length == 0) {
            setEncryptionKey(null);
        } else {
            try {
                byte[] keyBytes = generateKey(password, SALT, ITERATION_COUNT, KEY_LENGTH,
                        PBKD_ALGORITHM, PROVIDER);
                setEncryptionKey(keyBytes);
            } catch (GeneralSecurityException e) {
                throw new RuntimeException("Unable to encrypt data files.", e);
            }
        }
    }

    public void setEncryptionKey(byte[] encryptionKey) {
        if (encryptionKey == null || encryptionKey.length == 0) {
            saveKey(null);
        } else {
            SecretKey key = bytesToSecretKey(encryptionKey, AES_ALGORITHM);
            saveKey(key);
        }
    }

    protected SecretKey getSecretKey() {
        if (secretKey == null) {
            if (key != null) {
                setEncryptionKey(Base64Coder.decode(key));
            } else if (password != null) {
                setEncryptionPassword(password.toCharArray());
            }
        }
        return secretKey;
    }

    private void saveKey(SecretKey secretKey) {
        this.secretKey = secretKey;
        resetDelegateArchive();
    }

    private static byte[] generateKey(char[] password, byte[] salt, int iterationCount,
            int keyLength, String algorithm, String provider)
            throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchProviderException {
        SecretKeyFactory skf = SecretKeyFactory.getInstance(algorithm, provider);
        KeySpec ks = new PBEKeySpec(password, salt, iterationCount, keyLength);
        SecretKey sk = skf.generateSecret(ks);
        return sk.getEncoded();
    }

    private static SecretKey bytesToSecretKey(byte[] key, String algorithm) {
        return new SecretKeySpec(key, algorithm);
    }

}
