/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.config;

import android.util.Log;

import com.google.gson.JsonObject;

import io.kolektor.android.osf.OsfService;
import io.kolektor.android.osf.util.EqualsUtil;
import io.kolektor.android.osf.util.LogUtil;

public abstract class ConfigUpdater {

    public void run(String name, OsfService osfService) {
        JsonObject oldConfig = osfService.getPipelineConfig(name);
        try {
            JsonObject newConfig = getConfig();
            if (!EqualsUtil.areEqual(oldConfig, newConfig)) {
                osfService.saveAndReload(name, newConfig);
            }
        } catch (ConfigUpdateException e) {
            Log.w(LogUtil.TAG, "Unable to get config", e);
        }
    }

    abstract protected JsonObject getConfig() throws ConfigUpdateException;

    public class ConfigUpdateException extends Exception {

        private static final long serialVersionUID = 7595505577357891121L;

        public ConfigUpdateException() {
            super();
        }

        public ConfigUpdateException(String detailMessage, Throwable throwable) {
            super(detailMessage, throwable);
        }

        public ConfigUpdateException(String detailMessage) {
            super(detailMessage);
        }

        public ConfigUpdateException(Throwable throwable) {
            super(throwable);
        }

    }

}
