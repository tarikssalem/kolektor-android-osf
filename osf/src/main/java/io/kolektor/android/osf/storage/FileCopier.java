/*
 * Copyright (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.storage;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.util.zip.GZIPOutputStream;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import io.kolektor.android.osf.util.IOUtil;

import static io.kolektor.android.osf.util.LogUtil.TAG;

public interface FileCopier {

    /**
     * Copy file from source to destination.
     * FileCopier may modify content when copying or have other side effects.
     *
     * @param sourceFile
     * @param destinationFile
     * @return <code>true</code> if successfully copied, otherwise <code>false</code>
     */
    public boolean copy(File sourceFile, File destinationFile);

    /**
     * Returns a recommended filename extension of the copy.
     *
     * @return recommended filename extension of the copy
     */
    public String getRecommendedFilenameExtension();

    /* ------------------------------------------------------------------------------------------ */

    /**
     * Makes a copy of a file.
     */
    public static class SimpleFileCopier implements FileCopier {

        private static final String FILENAME_EXTENSION = "";

        @Override
        public boolean copy(File srcFile, File dstFile) {
            Log.d(TAG, "SimpleFileCopier: \"" + srcFile.getPath() + "\" to \"" + dstFile.getPath() + "\"");

            try {
                if (srcFile.exists()) {
                    dstFile.getParentFile().mkdirs();
                    FileChannel src = new FileInputStream(srcFile).getChannel();
                    FileChannel dst = new FileOutputStream(dstFile).getChannel();
                    try {
                        dst.transferFrom(src, 0, src.size());
                    } catch (IOException e) {
                        Log.e(TAG, "SimpleFileCopier: Error backing up file.", e);
                        return false;
                    } finally {
                        try {
                            if (src != null) {
                                src.close();
                            }
                            if (dst != null) {
                                dst.close();
                            }
                        } catch (IOException e) {
                            Log.e(TAG, "SimpleFileCopier: Error closing db files.", e);
                            return false;
                        }
                    }
                } else {
                    Log.e(TAG, "SimpleFileCopier: File does not exist: " + srcFile.getAbsolutePath());
                    return false;
                }
            } catch (FileNotFoundException e) {
                Log.e(TAG, "SimpleFileCopier: Unable to create backup.", e);
                return false;
            }

            Log.d(TAG, "SimpleFileCopier: Done.");
            return true;
        }

        @Override
        public String getRecommendedFilenameExtension() {
            return FILENAME_EXTENSION;
        }

    }

    /* ------------------------------------------------------------------------------------------ */

    /**
     * Makes an encrypted copy of a file.
     */
    public static class EncryptedFileCopier implements FileCopier {

        private static final String FILENAME_EXTENSION = ".enc";

        private final SecretKey key;
        private final String transformation;
        private final String provider;
        private Cipher cipher;

        public EncryptedFileCopier(SecretKey key, String transformation, String provider) {
            this.key = key;
            this.transformation = transformation;
            this.provider = provider;
        }

        protected Cipher getCipher() {
            if (cipher == null) {
                synchronized (this) {
                    if (cipher == null) {
                        try {
                            cipher = Cipher.getInstance(transformation, provider);
                            cipher.init(Cipher.ENCRYPT_MODE, key);
                        } catch (Exception e) {
                            Log.e(TAG, "EncryptedFileCopier: Error creating cipher.", e);
                        }
                    }
                }
            }
            return cipher;
        }

        @Override
        public boolean copy(File srcFile, File dstFile) {
            Log.d(TAG, "EncryptedFileCopier: \"" + srcFile.getPath() + "\" to \"" + dstFile.getPath() + "\"");

            Cipher cipher = getCipher();
            if (cipher == null) {
                return false;
            }

            InputStream in = null;
            OutputStream out = null;
            CipherOutputStream co = null;
            try {
                in = new FileInputStream(srcFile);
                out = new FileOutputStream(dstFile);
                co = new CipherOutputStream(out, cipher);
                byte[] buf = new byte[128 * 4096];
                int len;
                while ((len = in.read(buf)) > 0) {
                    co.write(buf, 0, len);
                }
            } catch (FileNotFoundException e) {
                Log.e(TAG, "EncryptedFileCopier: File not found.", e);
                return false;
            } catch (IOException e) {
                Log.e(TAG, "EncryptedFileCopier: Error while copying.", e);
                return false;
            } finally {
                IOUtil.close(in);
                IOUtil.close(co);
                IOUtil.close(out);
            }

            Log.d(TAG, "EncryptedFileCopier: Done.");
            return true;
        }

        @Override
        public String getRecommendedFilenameExtension() {
            return FILENAME_EXTENSION;
        }

    }

    /* ------------------------------------------------------------------------------------------ */

    /**
     * Makes an encrypted and compressed copy of a file.
     *
     * This class was taken and modified from https://github.com/OpenSensing/funf-core-android.
     * GNU LESSER GENERAL PUBLIC LICENSE, Version 3, 29 June 2007.
     */
    public static class CompressedEncryptedFileCopier implements FileCopier {

        private static final String FILENAME_EXTENSION = ".gz.enc";

        private final SecretKey key;
        private final String transformation;
        private final String provider;
        private Cipher cipher;

        public CompressedEncryptedFileCopier(SecretKey key, String transformation, String provider) {
            this.key = key;
            this.transformation = transformation;
            this.provider = provider;
        }

        protected Cipher getCipher() {
            if (cipher == null) {
                synchronized (this) {
                    if (cipher == null) {
                        try {
                            cipher = Cipher.getInstance(transformation, provider);
                            cipher.init(Cipher.ENCRYPT_MODE, key);
                        } catch (Exception e) {
                            Log.e(TAG, "CompressedEncryptedFileCopier: Error creating cipher.", e);
                        }
                    }
                }
            }
            return cipher;
        }

        @Override
        public boolean copy(File srcFile, File dstFile) {
            Log.d(TAG, "CompressedEncryptedFileCopier: \"" + srcFile.getPath() + "\" to \"" + dstFile.getPath() + "\"");

            Cipher cipher = getCipher();
            if (cipher == null) {
                return false;
            }

            InputStream in = null;
            OutputStream out = null;
            CipherOutputStream co = null;
            GZIPOutputStream gzipOutputStream = null;
            try {
                in = new FileInputStream(srcFile);
                out = new FileOutputStream(dstFile);
                co = new CipherOutputStream(out, cipher);
                gzipOutputStream = new GZIPOutputStream(co);
                byte[] buf = new byte[128 * 4096];
                int len;
                while ((len = in.read(buf)) > 0) {
                    Log.d(TAG, "CompressedEncryptedFileCopier: Compressing...");
                    gzipOutputStream.write(buf, 0, len);
                }
                gzipOutputStream.finish();

            } catch (FileNotFoundException e) {
                Log.e(TAG, "CompressedEncryptedFileCopier: File not found.", e);
                return false;
            } catch (IOException e) {
                Log.e(TAG, "CompressedEncryptedFileCopier: Error while copying.", e);
                return false;
            } finally {
                IOUtil.close(in);
                IOUtil.close(co);
                IOUtil.close(out);
            }

            Log.d(TAG, "CompressedEncryptedFileCopier: Done.");
            return true;
        }

        @Override
        public String getRecommendedFilenameExtension() {
            return FILENAME_EXTENSION;
        }

    }

    /* ------------------------------------------------------------------------------------------ */

    /**
     * Makes a compressed copy of a file.
     *
     * This class was taken and modified from https://github.com/OpenSensing/funf-core-android.
     * GNU LESSER GENERAL PUBLIC LICENSE, Version 3, 29 June 2007.
     */
    public static class CompressedFileCopier implements FileCopier {

        private static final String FILENAME_EXTENSION = ".gz";

        @Override
        public boolean copy(File srcFile, File dstFile) {
            Log.d(TAG, "CompressedFileCopier: \"" + srcFile.getPath() + "\" to \"" + dstFile.getPath() + "\"");

            InputStream in = null;
            OutputStream out = null;
            GZIPOutputStream gzipOutputStream = null;
            try {
                in = new FileInputStream(srcFile);
                out = new FileOutputStream(dstFile);
                gzipOutputStream = new GZIPOutputStream(out);
                byte[] buf = new byte[128 * 4096];
                int len;
                while ((len = in.read(buf)) > 0) {
                    Log.d(TAG, "CompressedFileCopier: Compressing...");
                    gzipOutputStream.write(buf, 0, len);
                }

            } catch (FileNotFoundException e) {
                Log.e(TAG, "CompressedFileCopier: File not found.", e);
                return false;
            } catch (IOException e) {
                Log.e(TAG, "CompressedFileCopier: Error while copying.", e);
                return false;
            } finally {
                IOUtil.close(in);
                IOUtil.close(gzipOutputStream);
                IOUtil.close(out);
            }

            Log.d(TAG, "CompressedFileCopier: Done.");
            return true;
        }

        @Override
        public String getRecommendedFilenameExtension() {
            return FILENAME_EXTENSION;
        }

    }

    /* ------------------------------------------------------------------------------------------ */

    /**
     * This file copier makes an encrypted and compressed copy of a file.
     * <p>
     * The copy contains an RSA encrypted random AES key followed by Gzip compressed and
     * AES encrypted data. It proceeds as follows:
     * <ol>
     * <li>Generate a random AES key.
     * <li>Encrypt the AES key using given RSA public key.
     * <li>Embed the encrypted AES key at the very beginning of the copy.
     * <li>Encrypt compressed data using the AES key.
     * </ol>
     */
    public static class GzipRsaAesFileCopier implements FileCopier {

        private static final String FILENAME_EXTENSION = ".gz.enc";
        private static final String RSA_TRANSFORMATION = "RSA/NONE/OAEPWithSHA256AndMGF1Padding";
        private static final int AES_KEY_LENGTH = 128;
        private static final String AES_ALGORITHM = "AES";
        private static final String AES_TRANSFORMATION = "AES/ECB/PKCS7Padding";

        private final PublicKey key;
        private final String provider;

        public GzipRsaAesFileCopier(PublicKey key, String provider) {
            this.key = key;
            this.provider = provider;
        }

        protected static SecretKey generateKey(int keyLength, String algorithm, String provider)
                throws NoSuchProviderException, NoSuchAlgorithmException {
            KeyGenerator keyGen = KeyGenerator.getInstance(algorithm, provider);
            keyGen.init(keyLength, new SecureRandom());
            return keyGen.generateKey();
        }

        protected static byte[] encrypt(byte[] data, PublicKey key, String transformation, String provider)
                throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException,
                InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
            Cipher cipher = Cipher.getInstance(transformation, provider);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return cipher.doFinal(data);
        }

        @Override
        public boolean copy(File srcFile, File dstFile) {
            Log.d(TAG, "GzipRsaAesFileCopier: \"" + srcFile.getPath() + "\" to \"" + dstFile.getPath() + "\"");

            SecretKey randomKey;
            byte[] encryptedRandomKey;
            Cipher cipher;
            try {
                randomKey = generateKey(AES_KEY_LENGTH, AES_ALGORITHM, provider);
                encryptedRandomKey = encrypt(randomKey.getEncoded(), key, RSA_TRANSFORMATION, provider);
                cipher = Cipher.getInstance(AES_TRANSFORMATION, provider);
                cipher.init(Cipher.ENCRYPT_MODE, randomKey);
            } catch (GeneralSecurityException e) {
                Log.e(TAG, "GzipRsaAesFileCopier: Error while copying...", e);
                return false;
            }

            InputStream in = null;
            OutputStream out = null;
            CipherOutputStream co = null;
            GZIPOutputStream gzipOutputStream;
            try {
                in = new FileInputStream(srcFile);
                out = new FileOutputStream(dstFile);
                co = new CipherOutputStream(out, cipher);
                gzipOutputStream = new GZIPOutputStream(co);
                out.write(encryptedRandomKey);
                byte[] buf = new byte[128 * 4096];
                int len;
                while ((len = in.read(buf)) > 0) {
                    Log.d(TAG, "GzipRsaAesFileCopier: Compressing...");
                    gzipOutputStream.write(buf, 0, len);
                }
                gzipOutputStream.finish();

            } catch (FileNotFoundException e) {
                Log.e(TAG, "GzipRsaAesFileCopier: File not found.", e);
                return false;
            } catch (IOException e) {
                Log.e(TAG, "GzipRsaAesFileCopier: Error while copying.", e);
                return false;
            } finally {
                IOUtil.close(in);
                IOUtil.close(co);
                IOUtil.close(out);
            }

            Log.d(TAG, "GzipRsaAesFileCopier: Done.");
            return true;
        }

        @Override
        public String getRecommendedFilenameExtension() {
            return FILENAME_EXTENSION;
        }

    }

}
