/*
 * Copyright (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.storage;

import android.content.Context;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.crypto.SecretKey;

import io.kolektor.android.osf.util.LogUtil;
import io.kolektor.android.osf.util.NameGenerator;

public class LargeFileArchive extends DefaultArchive {

    private static final String ARCHIVE_DIR_SUFFIX = "_archiveLarge";
    private static final String BACKUP_DIR_SUFFIX = "_backupLarge";

    private FileArchive largeFileArchive;

    public LargeFileArchive(Context context, String name) {
        super(context, name);
    }

    protected FileArchive getLargeFileArchive() {
        if (largeFileArchive == null) {
            synchronized (this) {
                if (largeFileArchive == null) {
                    SecretKey key = getSecretKey();

                    File backupArchiveDir = getBackupArchiveDir();
                    FileArchive backupArchive = FileDirectoryArchive.getRollingFileArchive(backupArchiveDir);

                    File mainArchiveDir = getExternalArchiveDir();
                    NameGenerator nameGenerator = new NameGenerator.IdentityNameGenerator();
                    FileCopier copier = (key == null) ? new FileCopier.SimpleFileCopier() :
                            new FileCopier.EncryptedFileCopier(key, TRANSFORMATION, PROVIDER);
                    DirectoryCleaner cleaner = new DirectoryCleaner.KeepAll();
                    FileArchive mainArchive = new FileDirectoryArchive(mainArchiveDir, nameGenerator,
                            copier, cleaner);

                    largeFileArchive = new BackedUpArchive(mainArchive, backupArchive);
                }
            }
        }
        return largeFileArchive;
    }

    // TODO: Don't get it here.
    protected boolean isLargeFile(File item) {
        String extension = MimeTypeMap.getFileExtensionFromUrl(item.getAbsolutePath());
        if (extension == null)
            return false;
        String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        if (type == null || "".equals(type) || "null".equals(type))
            return false;
        else
            return true;
    }

    protected String getArchiveDirName() {
        return ARCHIVE_DIR_PREFIX + getSanitizedName() + ARCHIVE_DIR_SUFFIX;
    }

    protected String getBackupDirName() {
        return ARCHIVE_DIR_PREFIX + getSanitizedName() + BACKUP_DIR_SUFFIX;
    }

    @Override
    public boolean add(File item) {
        if (isLargeFile(item)) {
            Log.d(LogUtil.TAG, "adding to large archive");
            return getLargeFileArchive().add(item);
        } else
            return getDelegateArchive().add(item);
    }

    @Override
    public boolean contains(File item) {
        if (isLargeFile(item))
            return getLargeFileArchive().contains(item);
        else
            return getDelegateArchive().contains(item);
    }

    @Override
    public File[] getAll() {
        List<File> dbFiles = Arrays.asList(getDelegateArchive().getAll());
        List<File> largeFiles = Arrays.asList(getLargeFileArchive().getAll());
        List<File> allFiles = new ArrayList<File>();
        allFiles.addAll(dbFiles);
        allFiles.addAll(largeFiles);
        File[] allFilesArray = new File[allFiles.size()];
        return allFiles.toArray(allFilesArray);
    }

    @Override
    public boolean remove(File item) {
        if (isLargeFile(item))
            return getLargeFileArchive().remove(item);
        else
            return getDelegateArchive().remove(item);
    }

}
