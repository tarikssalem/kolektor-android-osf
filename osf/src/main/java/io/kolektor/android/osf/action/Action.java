/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.action;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

public class Action implements Runnable {

    private Looper mLooper = null;
    private Handler mHandler = null;

    Action() {
    }

    protected Handler getHandler() {
        return mHandler;
    }

    public void setHandler(Handler handler) {
        if (mHandler != null) {
            exitHandler();
        }
        mHandler = handler;
    }

    @Override
    public final void run() {
        if (isLongRunningAction()) {
            ensureHandlerExists();
            if (Looper.myLooper() != getHandler().getLooper()) {
                getHandler().post(this);
                return;
            }
        }
        execute();
    }

    /**
     * Override this function to include the action-specific code.
     */
    protected void execute() {
        // Perform action here.
    }

    protected boolean isLongRunningAction() {
        return false;
    }

    protected void ensureHandlerExists() {
        if (mHandler == null) {
            synchronized (this) {
                if (mLooper == null) {
                    HandlerThread thread = new HandlerThread(getClass().getName());
                    thread.start();
                    mLooper = thread.getLooper();
                    mHandler = new Handler(mLooper);
                }
            }
        }
    }

    protected void exitHandler() {
        if (mHandler != null) {
            synchronized (this) {
                if (mLooper != null) {
                    mLooper.quit();
                    mLooper = null;
                    mHandler = null;
                }
            }
        }
    }

}
