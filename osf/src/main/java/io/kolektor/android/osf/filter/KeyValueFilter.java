/**
 * 
 * Funf: Open Sensing Framework
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland.
 * Acknowledgments: Alan Gardner
 * Contact: nadav@media.mit.edu
 * 
 * Author(s): Pararth Shah (pararthshah717@gmail.com)
 * 
 * This file is part of Funf.
 * 
 * Funf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * Funf is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with Funf. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package io.kolektor.android.osf.filter;

import java.util.Map;

import com.google.gson.JsonElement;

import io.kolektor.android.osf.config.Configurable;
import io.kolektor.android.osf.datasource.DataSource;
import io.kolektor.android.osf.json.IJsonObject;
import io.kolektor.android.osf.probe.Probe;

public class KeyValueFilter implements Probe.DataListener, DataSource {
    
    @Configurable
    private Probe.DataListener listener;
        
    @Configurable
    private Map<String, String> matches = null;
    
    KeyValueFilter() {
    }
    
    public KeyValueFilter(Probe.DataListener listener) {
        this.listener = listener;
    }

    @Override
    public void onDataReceived(IJsonObject probeConfig, IJsonObject data) {
        if (listener == null)
            return;
        
        if (matches != null) {
            for (String key: matches.keySet()) {
                if (data.has(key)) {
                    String dataValue = data.get(key).getAsString();
                    if (dataValue.equals(matches.get(key))) {
                        listener.onDataReceived(probeConfig, data);
                        return;
                    }
                }
            }
        }
    }

    @Override
    public void onDataCompleted(IJsonObject probeConfig, JsonElement checkpoint) {
        listener.onDataCompleted(probeConfig, checkpoint);
    }

    @Override
    public void setListener(Probe.DataListener listener) {
        this.listener = listener;
    }

}
