/**
 * 
 * Funf: Open Sensing Framework
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland.
 * Acknowledgments: Alan Gardner
 * Contact: nadav@media.mit.edu
 * 
 * Author(s): Pararth Shah (pararthshah717@gmail.com)
 * 
 * This file is part of Funf.
 * 
 * Funf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * Funf is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with Funf. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package io.kolektor.android.osf.probe.builtin;


import java.io.File;
import java.io.IOException;

import android.media.MediaRecorder;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;

import com.google.gson.JsonObject;

import io.kolektor.android.osf.Schedule;
import io.kolektor.android.osf.config.Configurable;
import io.kolektor.android.osf.time.TimeUtil;
import io.kolektor.android.osf.util.LogUtil;
import io.kolektor.android.osf.util.NameGenerator;
import io.kolektor.android.osf.probe.Probe;

@Probe.DisplayName("Audio Capture Probe")
@Probe.RequiredPermissions({android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.RECORD_AUDIO})
@Probe.RequiredFeatures("android.hardware.microphone")
@Schedule.DefaultSchedule(interval=1800)
public class AudioCaptureProbe extends ImpulseProbe implements Probe.PassiveProbe, ProbeKeys.HighBandwidthKeys {

    @Configurable
    private String fileNameBase = "audiorectest";

    @Configurable
    private String folderName = "myaudios";
    
    @Configurable
    private int recordingLength = 5; // Duration of recording in seconds

    private String mFileName;
    private String mFolderPath;
    
    private MediaRecorder mRecorder;    
    private NameGenerator mNameGenerator;

    private class RecordingCountDown extends CountDownTimer {

        public RecordingCountDown(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            stopRecording();
        }

        @Override
        public void onTick(long millisUntilFinished) {
            //Log.d(LogUtil.TAG, "Audio capture: seconds remaining = " + millisUntilFinished / 1000);
        }
    }

    private RecordingCountDown mCountDown;

    @Override
    protected void onEnable() {
        super.onEnable();
        mNameGenerator = new NameGenerator.SystemUniqueTimestampNameGenerator(getContext());
        mFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() 
                + "/" + folderName;
        File folder = new File(mFolderPath);
        if (!folder.exists()) {
            folder.mkdirs();
        } else if (!folder.isDirectory()) {
            folder.delete();
            folder.mkdirs();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LogUtil.TAG, "AudioCaptureProbe: Probe initialization");
        mFileName = mFolderPath + "/" + mNameGenerator.generateName(fileNameBase) + ".mp4";
        
        mCountDown = new RecordingCountDown(TimeUtil.secondsToMillis(recordingLength), 1000);
        if (startRecording())
            mCountDown.start();
        else {
            abortRecording();
        }
    }
    
    private boolean startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
            Log.d(LogUtil.TAG, "AudioCaptureProbe: Recording audio start");
            mRecorder.start();
        } catch (IOException e) {
            Log.e(LogUtil.TAG, "AudioCaptureProbe: Error in preparing MediaRecorder");
            Log.e(LogUtil.TAG, e.getLocalizedMessage());
            return false;
        }
        
        return true;
    }

    private void stopRecording() {
        mRecorder.stop();
        mRecorder.reset();
        mRecorder.release();
        mRecorder = null;
        
        Log.d(LogUtil.TAG, "AudioCaptureProbe: Recording audio stop");
        JsonObject data = new JsonObject();
        data.addProperty(FILENAME, mFileName);
        sendData(data);
        stop();
    }
    
    private void abortRecording() {
        Log.e(LogUtil.TAG, "AudioCaptureProbe: Recording audio abort");
        mRecorder.reset();
        mRecorder.release();
        mRecorder = null;
        stop();
    }

}
