/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.TypeAdapterFactory;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.kolektor.android.osf.action.Action;
import io.kolektor.android.osf.config.ConfigUpdater;
import io.kolektor.android.osf.config.ConfigurableTypeAdapterFactory;
import io.kolektor.android.osf.config.ContextInjectorTypeAdapaterFactory;
import io.kolektor.android.osf.config.DefaultRuntimeTypeAdapterFactory;
import io.kolektor.android.osf.config.DefaultScheduleSerializer;
import io.kolektor.android.osf.config.HttpConfigUpdater;
import io.kolektor.android.osf.config.ListenerInjectorTypeAdapterFactory;
import io.kolektor.android.osf.config.SingletonTypeAdapterFactory;
import io.kolektor.android.osf.datasource.Startable;
import io.kolektor.android.osf.pipeline.Pipeline;
import io.kolektor.android.osf.pipeline.PipelineFactory;
import io.kolektor.android.osf.probe.Probe;
import io.kolektor.android.osf.storage.DefaultArchive;
import io.kolektor.android.osf.storage.FileArchive;
import io.kolektor.android.osf.storage.HttpArchive;
import io.kolektor.android.osf.storage.RemoteFileArchive;
import io.kolektor.android.osf.util.StringUtil;

import static io.kolektor.android.osf.util.LogUtil.TAG;

/**
 * The pipeline configuration is read from the service's metadata in the manifest.
 */
public class OsfService extends Service {

    public static final String ACTION_KEEP_ALIVE = "osf.keepalive";
    public static final String ACTION_INTERNAL = "osf.internal";

    public static final String PIPELINE_TYPE = "osf/pipeline";
    public static final String ALARM_TYPE = "osf/alarm";

    private static final String DISABLED_PIPELINE_LIST = "__DISABLED__";
    private static final String OSF_SCHEME = "osf";

    private static Class sServiceClass;
    private static PipelineFactory sPipelineFactory;
    private static SingletonTypeAdapterFactory sProbeFactory;
    private static DefaultRuntimeTypeAdapterFactory<Action> sActionFactory;
    private static ListenerInjectorTypeAdapterFactory sDataSourceFactory;

    private OsfBinder mOsfBinder = new OsfBinder();
    private Handler mHandler;
    private SharedPreferences mPrefs;
    private Map<String, Pipeline> mPipelines;
    private Map<String, Pipeline> mDisabledPipelines;
    private Set<String> mDisabledPipelineNames;
    private Gson mGson;

    public OsfService() {
        Log.d(TAG, "OsfService: OsfService()");
        sServiceClass = this.getClass();
    }

    /**
     * Get a Gson builder with the probe factory built in
     *
     * @return
     */
    public static GsonBuilder getGsonBuilder(Context context) {
        return new GsonBuilder()
                .registerTypeAdapterFactory(getProbeFactory(context))
                .registerTypeAdapterFactory(getActionFactory(context))
                .registerTypeAdapterFactory(getPipelineFactory(context))
                .registerTypeAdapterFactory(getDataSourceFactory(context))
                .registerTypeAdapterFactory(new ConfigurableRuntimeTypeAdapterFactory<Schedule>(context, Schedule.class, Schedule.BasicSchedule.class))
                .registerTypeAdapterFactory(new ConfigurableRuntimeTypeAdapterFactory<ConfigUpdater>(context, ConfigUpdater.class, HttpConfigUpdater.class))
                .registerTypeAdapterFactory(new ConfigurableRuntimeTypeAdapterFactory<FileArchive>(context, FileArchive.class, DefaultArchive.class))
                .registerTypeAdapterFactory(new ConfigurableRuntimeTypeAdapterFactory<RemoteFileArchive>(context, RemoteFileArchive.class, HttpArchive.class))
                .registerTypeAdapterFactory(new ConfigurableRuntimeTypeAdapterFactory<Probe.DataListener>(context, Probe.DataListener.class, null))
                .registerTypeAdapter(Schedule.DefaultSchedule.class, new DefaultScheduleSerializer())
                .registerTypeAdapter(Class.class, new JsonSerializer<Class<?>>() {
                    @Override
                    public JsonElement serialize(Class<?> src, Type typeOfSrc,
                            JsonSerializationContext context) {
                        return src == null ? JsonNull.INSTANCE : new JsonPrimitive(src.getName());
                    }
                });
    }

    public static PipelineFactory getPipelineFactory(Context context) {
        if (sPipelineFactory == null) {
            sPipelineFactory = new PipelineFactory(context);
        }
        return sPipelineFactory;
    }

    public static SingletonTypeAdapterFactory getProbeFactory(Context context) {
        if (sProbeFactory == null) {
            sProbeFactory = new SingletonTypeAdapterFactory(
                    new DefaultRuntimeTypeAdapterFactory<Probe>(
                            context,
                            Probe.class,
                            null,
                            new ContextInjectorTypeAdapaterFactory(context, new ConfigurableTypeAdapterFactory())));
        }
        return sProbeFactory;
    }

    public static DefaultRuntimeTypeAdapterFactory<Action> getActionFactory(Context context) {
        if (sActionFactory == null) {
            sActionFactory = new DefaultRuntimeTypeAdapterFactory<Action>(
                    context,
                    Action.class,
                    null,
                    new ContextInjectorTypeAdapaterFactory(context, new ConfigurableTypeAdapterFactory()));
        }
        return sActionFactory;
    }

    public static ListenerInjectorTypeAdapterFactory getDataSourceFactory(Context context) {
        if (sDataSourceFactory == null) {
            sDataSourceFactory = new ListenerInjectorTypeAdapterFactory(
                    new DefaultRuntimeTypeAdapterFactory<Startable>(
                            context,
                            Startable.class,
                            null,
                            new ContextInjectorTypeAdapaterFactory(context, new ConfigurableTypeAdapterFactory())));
        }
        return sDataSourceFactory;
    }

    public static void registerAlarm(Context context, String probeConfig, Long start, Long interval,
            boolean exact) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = getOsfIntent(context, ALARM_TYPE, probeConfig, "");
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (start == null)
            start = 0L;

        if (interval == null || interval <= 0) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, start, pendingIntent);
        } else {
            if (exact) {
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, start, interval, pendingIntent);
            } else {
                alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, start, interval, pendingIntent);

            }
        }
    }

    public static void unregisterAlarm(Context context, String probeConfig) {
        Intent intent = getOsfIntent(context, ALARM_TYPE, probeConfig, "");
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_NO_CREATE);
        if (pendingIntent != null) {
            pendingIntent.cancel();
        }
    }

    // TODO: should these public?  May be confusing for people just using the library
    private static Uri getComponentUri(String component, String action) {
        return new Uri.Builder()
                .scheme(OSF_SCHEME)
                .path(component) // Automatically prepends slash
                .fragment(action)
                .build();
    }

    private static String getComponentName(Uri componentUri) {
        return componentUri.getPath().substring(1); // Remove automatically prepended slash from beginning
    }

    private static String getAction(Uri componentUri) {
        return componentUri.getFragment();
    }

    private static Intent getOsfIntent(Context context, String type, String component,
            String action) {
        return getOsfIntent(context, type, getComponentUri(component, action));
    }

    private static Intent getOsfIntent(Context context, String type, Uri componentUri) {
        Intent intent = new Intent();
        intent.setClass(context, sServiceClass);
        intent.setPackage(context.getPackageName());
        intent.setAction(ACTION_INTERNAL);
        intent.setDataAndType(componentUri, type);
        return intent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "OsfService: onCreate()");
        this.mHandler = new Handler();
        getGson(); // Sets gson
        this.mPrefs = getSharedPreferences(getClass().getName(), MODE_PRIVATE);
        this.mPipelines = new HashMap<String, Pipeline>();
        this.mDisabledPipelines = new HashMap<String, Pipeline>();
        this.mDisabledPipelineNames = new HashSet<String>(Arrays.asList(mPrefs.getString(DISABLED_PIPELINE_LIST, "").split(",")));
        this.mDisabledPipelineNames.remove(""); // Remove the empty name, if no disabled pipelines exist
        reload();
    }

    public void reload() {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    reload();
                }
            });
            return;
        }
        Set<String> pipelineNames = new HashSet<String>();
        pipelineNames.addAll(mPrefs.getAll().keySet());
        pipelineNames.remove(DISABLED_PIPELINE_LIST);
        Bundle metadata = getMetadata();
        pipelineNames.addAll(metadata.keySet());
        for (String pipelineName : pipelineNames) {
            reload(pipelineName);
        }
    }

    public void reload(final String name) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    reload(name);
                }
            });
            return;
        }
        String pipelineConfig = null;
        Bundle metadata = getMetadata();
        if (mPrefs.contains(name)) {
            pipelineConfig = mPrefs.getString(name, null);
        } else if (metadata.containsKey(name)) {
            pipelineConfig = metadata.getString(name);
        }
        if (mDisabledPipelineNames.contains(name)) {
            // Disabled, so don't load any config
            Pipeline disabledPipeline = mGson.fromJson(pipelineConfig, Pipeline.class);
            mDisabledPipelines.put(name, disabledPipeline);
            pipelineConfig = null;
        }
        if (pipelineConfig == null) {
            unregisterPipeline(name);
        } else {
            Pipeline newPipeline = mGson.fromJson(pipelineConfig, Pipeline.class);
            registerPipeline(name, newPipeline); // Will unregister previous before running
        }
    }

    public JsonObject getPipelineConfig(String name) {
        String configString = mPrefs.getString(name, null);
        Bundle metadata = getMetadata();
        if (configString == null && metadata.containsKey(name)) {
            configString = metadata.getString(name);
        }
        return configString == null ? null : new JsonParser().parse(configString).getAsJsonObject();
    }

    public boolean save(String name, JsonObject config) {
        try {
            // Check if this is a valid pipeline before saving
            Pipeline pipeline = getGson().fromJson(config, Pipeline.class);
            return mPrefs.edit().putString(name, config.toString()).commit();
        } catch (Exception e) {
            Log.e(TAG, "Unable to save config: " + config.toString());
            return false;
        }
    }

    public boolean saveAndReload(String name, JsonObject config) {
        boolean success = save(name, config);
        if (success) {
            reload(name);
        }
        return success;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "OsfService: onDestroy()");

        // TODO: call onDestroy on all pipelines
        for (Pipeline pipeline : mPipelines.values()) {
            pipeline.onDestroy();
        }

        // TODO: save outstanding requests
        // TODO: remove all remaining Alarms

        // TODO: make sure to destroy all probes
        for (Object probeObject : getProbeFactory().getCached()) {
            //String componentString = JsonUtils.immutable(gson.toJsonTree(probeObject)).toString();
            //cancelProbe(componentString);
            ((Probe) probeObject).destroy();
        }
        getProbeFactory().clearCache();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "OsfService: onStartCommand()");
        String action = intent == null ? null : intent.getAction();
        if (action == null || ACTION_KEEP_ALIVE.equals(action)) {
            // Does nothing, but wakes up OsfService
        } else if (ACTION_INTERNAL.equals(action)) {
            String type = intent.getType();
            Uri componentUri = intent.getData();
            if (PIPELINE_TYPE.equals(type)) {
                // Handle pipeline action
                String pipelineName = getComponentName(componentUri);
                String pipelineAction = getAction(componentUri);
                Pipeline pipeline = mPipelines.get(pipelineName);
                if (pipeline != null) {
                    pipeline.onRun(pipelineAction, null);
                }
            } else if (ALARM_TYPE.equals(type)) {
                // Handle registered alarms
                String probeConfig = getComponentName(componentUri);
                final Probe probe = getGson().fromJson(probeConfig, Probe.class);
                if (probe instanceof Runnable) {
                    mHandler.post((Runnable) probe);
                }
            }

        }
        return Service.START_STICKY;
    }

    private Bundle getMetadata() {
        try {
            Bundle metadata = getPackageManager().getServiceInfo(new ComponentName(this, this.getClass()), PackageManager.GET_META_DATA).metaData;
            return metadata == null ? new Bundle() : metadata;
        } catch (NameNotFoundException e) {
            throw new RuntimeException("Unable to get metadata for the OsfService service.");
        }
    }

    /**
     * Get a gson builder with the probe factory built in
     *
     * @return
     */
    public GsonBuilder getGsonBuilder() {
        return getGsonBuilder(this);
    }

    /**
     * Get a Gson instance which includes the SingletonProbeFactory
     *
     * @return
     */
    public Gson getGson() {
        if (mGson == null) {
            mGson = getGsonBuilder().create();
        }
        return mGson;
    }

    public TypeAdapterFactory getPipelineFactory() {
        return getPipelineFactory(this);
    }

    public SingletonTypeAdapterFactory getProbeFactory() {
        return getProbeFactory(this);
    }

    public DefaultRuntimeTypeAdapterFactory<Action> getActionFactory() {
        return getActionFactory(this);
    }

    public ListenerInjectorTypeAdapterFactory getDataSourceFactory() {
        return getDataSourceFactory(this);
    }

    public void registerPipeline(String name, Pipeline pipeline) {
        synchronized (mPipelines) {
            Log.d(TAG, "Registering pipeline: " + name);
            unregisterPipeline(name);
            mPipelines.put(name, pipeline);
            pipeline.onCreate(this);
        }
    }

    public Pipeline getRegisteredPipeline(String name) {
        Pipeline p = mPipelines.get(name);
        if (p == null) {
            p = mDisabledPipelines.get(name);
        }
        return p;
    }

    public void unregisterPipeline(String name) {
        synchronized (mPipelines) {
            Pipeline existingPipeline = mPipelines.remove(name);
            if (existingPipeline != null) {
                existingPipeline.onDestroy();
            }
        }
    }

    public void enablePipeline(String name) {
        boolean previouslyDisabled = mDisabledPipelineNames.remove(name);
        if (previouslyDisabled) {
            mPrefs.edit().putString(DISABLED_PIPELINE_LIST, StringUtil.join(mDisabledPipelineNames, ",")).commit();
            reload(name);
        }
    }

    /////////////////////////////////////////////
    // Reserve action for later inter-osf communication
    // Use type to differentiate between probe/pipeline
    // osf:<componenent_name>#<action>

    public boolean isEnabled(String name) {
        return this.mPipelines.containsKey(name) && !mDisabledPipelineNames.contains(name);
    }

    public void disablePipeline(String name) {
        boolean previouslyEnabled = mDisabledPipelineNames.add(name);
        if (previouslyEnabled) {
            mPrefs.edit().putString(DISABLED_PIPELINE_LIST, StringUtil.join(mDisabledPipelineNames, ",")).commit();
            reload(name);
        }
    }

    private String getPipelineName(Pipeline pipeline) {
        for (Map.Entry<String, Pipeline> entry : mPipelines.entrySet()) {
            if (entry.getValue() == pipeline) {
                return entry.getKey();
            }
        }
        return null;
    }

    public static class ConfigurableRuntimeTypeAdapterFactory<E> extends DefaultRuntimeTypeAdapterFactory<E> {

        public ConfigurableRuntimeTypeAdapterFactory(Context context, Class<E> baseClass,
                Class<? extends E> defaultClass) {
            super(context,
                    baseClass,
                    defaultClass,
                    new ContextInjectorTypeAdapaterFactory(context, new ConfigurableTypeAdapterFactory()));
        }

    }

    // ---------------------------------------------------------------------------------------------

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "OsfService: onBind()");
        return mOsfBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "OsfService: onUnbind()");
        return super.onUnbind(intent);
    }

    public class OsfBinder extends Binder {
        public OsfService getService() {
            return OsfService.this;
        }
    }

}
