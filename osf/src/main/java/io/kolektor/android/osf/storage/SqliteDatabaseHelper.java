/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import io.kolektor.android.osf.json.IJsonObject;
import io.kolektor.android.osf.time.TimeUtil;
import io.kolektor.android.osf.util.StringUtil;
import io.kolektor.android.osf.util.UuidUtil;

/**
 * SQLite data storage.
 */
public class SqliteDatabaseHelper extends SQLiteOpenHelper implements DatabaseHelper {

    public static final int CURRENT_VERSION = 1;

    public static final Table DATA_TABLE = new Table("data",
            Arrays.asList(new Column(COLUMN_NAME, "TEXT"), // ACTION from data broadcast
                    new Column(COLUMN_TIMESTAMP, "FLOAT"), // TIMESTAMP in data broadcast
                    new Column(COLUMN_VALUE, "TEXT"))); // JSON representing
    public static final String COLUMN_DATABASE_NAME = "dbname";
    public static final String COLUMN_INSTALLATION = "device";
    public static final String COLUMN_UUID = "uuid";
    public static final String COLUMN_CREATED = "created";
    public static final Table FILE_INFO_TABLE = new Table("file_info",
            Arrays.asList(new Column(COLUMN_DATABASE_NAME, "TEXT"), // Name of this database
                    new Column(COLUMN_INSTALLATION, "TEXT"), // Universally Unique Id for device installation
                    new Column(COLUMN_UUID, "TEXT"), // Universally Unique Id for file
                    new Column(COLUMN_CREATED, "FLOAT"))); // TIMESTAMP in data broadcast

    private static int size = 0;
    private final Context context;
    private final String databaseName;

    public SqliteDatabaseHelper(Context context, String name, int version) {
        super(context, name, null, version);
        this.context = context;
        this.databaseName = name;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATA_TABLE.getCreateTableSQL());
        db.execSQL(FILE_INFO_TABLE.getCreateTableSQL());
        // Insert file identifier information
        String installationUuid = UuidUtil.getInstallationId(context);
        String fileUuid = UUID.randomUUID().toString();
        double createdTime = TimeUtil.getTimestamp().doubleValue();
        db.execSQL(String.format(Locale.US,
                "insert into %s (%s, %s, %s, %s) values ('%s', '%s', '%s', %f)",
                FILE_INFO_TABLE.name,
                COLUMN_DATABASE_NAME, COLUMN_INSTALLATION, COLUMN_UUID, COLUMN_CREATED,
                databaseName, installationUuid, fileUuid, createdTime));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Nothing yet
    }

    @Override
    public void initialize() {
        this.getWritableDatabase();
    }

    @Override
    public void insert(String key, BigDecimal timestamp, IJsonObject data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(SqliteDatabaseHelper.COLUMN_NAME, key);
        cv.put(SqliteDatabaseHelper.COLUMN_TIMESTAMP, timestamp.toString());
        cv.put(SqliteDatabaseHelper.COLUMN_VALUE, data.toString());
        db.insertOrThrow(SqliteDatabaseHelper.DATA_TABLE.name, "", cv);
        size += 1;
    }

    @Override
    public void close() {
        this.getWritableDatabase().close();
    }

    @Override
    public boolean delete() {
        boolean deleted = new File(this.getPath()).delete();
        if (deleted) {
            size = 0;
        }
        return deleted;
    }

    @Override
    public String getPath() {
        return this.getWritableDatabase().getPath();
    }

    @Override
    public File getFile() {
        return new File(this.getPath());
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    /**
     * Immutable Table Definition
     */
    public static class Table {
        private static final String CREATE_TABLE_FORMAT = "CREATE TABLE %s (_id INTEGER primary key autoincrement, %s);";

        public final String name;
        private final List<Column> columns;

        public Table(final String name, final List<Column> columns) {
            this.name = name;
            this.columns = new ArrayList<Column>(columns);
        }

        public List<Column> getColumns() {
            return new ArrayList<Column>(columns);
        }

        public String getCreateTableSQL() {
            return String.format(CREATE_TABLE_FORMAT, name, StringUtil.join(columns, ", "));
        }
    }

    /**
     * Immutable Column Definition
     */
    public static class Column {
        public final String name, type;

        public Column(final String name, final String type) {
            this.name = name;
            this.type = type;
        }

        @Override
        public String toString() {
            return name + " " + type;
        }
    }

}
