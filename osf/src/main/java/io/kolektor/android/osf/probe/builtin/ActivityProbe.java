/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.probe.builtin;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.kolektor.android.osf.Schedule;
import io.kolektor.android.osf.config.Configurable;
import io.kolektor.android.osf.json.IJsonObject;
import io.kolektor.android.osf.probe.Probe;

@Schedule.DefaultSchedule(interval = 120, duration = 15)
@Probe.RequiredFeatures("android.hardware.sensor.accelerometer")
@Probe.RequiredProbes(AccelerometerSensorProbe.class)
public class ActivityProbe extends Probe.Base implements Probe.ContinuousProbe, ProbeKeys.ActivityKeys {

    @Configurable
    private double interval = 1.0;

    //private long startTime;
    private ActivityCounter activityCounter = new ActivityCounter();

    @Override
    protected void onStart() {
        super.onStart();
        getAccelerometerProbe().registerListener(activityCounter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        getAccelerometerProbe().unregisterListener(activityCounter);
    }

    private AccelerometerSensorProbe getAccelerometerProbe() {
        return getGson().fromJson(DEFAULT_CONFIG, AccelerometerSensorProbe.class);
    }

    /* ------------------------------------------------------------------------------------------ */

    private class ActivityCounter implements DataListener {

        private double intervalStartTime;
        private float varianceSum;
        private float avg;
        private float sum;
        private int count;

        private void reset() {
            // If more than an interval away, then start a new scan.
            varianceSum = avg = sum = count = 0;
        }

        private void intervalReset() {
            // Calculate activity and reset.
            JsonObject data = new JsonObject();
            if (varianceSum >= 10.0f) {
                data.addProperty(ACTIVITY_LEVEL, ACTIVITY_LEVEL_HIGH);
            } else if (varianceSum < 10.0f && varianceSum > 3.0f) {
                data.addProperty(ACTIVITY_LEVEL, ACTIVITY_LEVEL_LOW);
            } else {
                data.addProperty(ACTIVITY_LEVEL, ACTIVITY_LEVEL_NONE);
            }
            sendData(data);
            varianceSum = avg = sum = count = 0;
        }

        private void update(float x, float y, float z) {
            // Iteratively calculate variance sum.
            count++;
            float magnitude = (float) Math.sqrt(x * x + y * y + z * z);
            float newAvg = (count - 1) * avg / count + magnitude / count;
            float deltaAvg = newAvg - avg;
            varianceSum += (magnitude - newAvg) * (magnitude - newAvg)
                    - 2 * (sum - (count - 1) * avg)
                    + (count - 1) * (deltaAvg * deltaAvg);
            sum += magnitude;
            avg = newAvg;
        }

        @Override
        public void onDataReceived(IJsonObject completeProbeUri, IJsonObject data) {
            double timestamp = data.get(TIMESTAMP).getAsDouble();
            if (intervalStartTime == 0.0 || (timestamp >= intervalStartTime + 2 * interval)) {
                reset();
                intervalStartTime = timestamp;
            } else if (timestamp >= intervalStartTime + interval) {
                intervalReset();
                intervalStartTime = timestamp;
            }
            float x = data.get(AccelerometerSensorProbe.X).getAsFloat();
            float y = data.get(AccelerometerSensorProbe.Y).getAsFloat();
            float z = data.get(AccelerometerSensorProbe.Z).getAsFloat();
            update(x, y, z);
        }

        @Override
        public void onDataCompleted(IJsonObject completeProbeUri, JsonElement checkpoint) {
            // Do nothing.
        }
    }

}
