/*
 * Copyright (C) 2017 Tárik S. Salem.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.storage;

import android.content.Context;
import android.database.SQLException;
import android.util.Log;

import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;

import io.kolektor.android.osf.json.IJsonObject;
import io.kolektor.android.osf.util.LogUtil;

import static io.kolektor.android.osf.util.LogUtil.TAG;

/**
 * JSON data storage.
 */
public class JsonDatabaseHelper implements DatabaseHelper {

    private static FileOutputStream fos = null;
    private static int size = 0;
    private Context context;
    private String databaseName;

    public JsonDatabaseHelper(Context context, String name) {
        this.context = context;
        this.databaseName = name;
    }

    @Override
    public void initialize() {
        try {
            fos = this.context.openFileOutput(this.databaseName, Context.MODE_APPEND);
        } catch (IOException e) {
            Log.e(TAG, "JsonDatabaseHelper: Failed to create or open file. path = [" +
                    getPath() + "]", e);
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void insert(String key, BigDecimal timestamp, IJsonObject data) {
        if (fos == null) initialize();
        try {
            JsonObject dataObject = new JsonObject();
            dataObject.addProperty(COLUMN_NAME, key);
            dataObject.addProperty(COLUMN_TIMESTAMP, timestamp);
            dataObject.add(COLUMN_VALUE, data);
            fos.write((dataObject.toString() + "\n").getBytes());
            size += 1;
        } catch (IOException e) {
            Log.e(TAG, "JsonDatabaseHelper: Failed to write data to file. path = [" +
                    getPath() + "]", e);
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void close() {
        try {
            if (fos != null) {
                fos.close();
            }
        } catch (IOException e) {
            Log.e(TAG, "JsonDatabaseHelper: Failed to close file. path = [" +
                    getPath() + "]", e);
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean delete() {
        boolean deleted = new File(this.getPath()).delete();
        if (deleted) {
            size = 0;
        }
        return deleted;
    }

    @Override
    public String getPath() {
        return this.context.getFileStreamPath(this.databaseName).getPath();
    }

    @Override
    public File getFile() {
        return new File(this.getPath());
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

}
