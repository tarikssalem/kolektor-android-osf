/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.action;

import com.google.gson.JsonElement;

import io.kolektor.android.osf.config.Configurable;
import io.kolektor.android.osf.json.IJsonObject;
import io.kolektor.android.osf.datasource.Startable;
import io.kolektor.android.osf.probe.Probe;

public class ActionAdapter implements Probe.DataListener {
    
    @Configurable
    private Action target = null;
        
    ActionAdapter() {
    }
    
    public ActionAdapter(Action action) {
        this.target = action;
    }
    
    public ActionAdapter(Startable.TriggerAction action) {
        this.target = (Action)action;
    }
    
    @Override
    public void onDataReceived(IJsonObject dataSourceConfig, IJsonObject data) {
        if (target != null) {
            target.run();
        }
    }

    @Override
    public void onDataCompleted(IJsonObject dataSourceConfig, JsonElement checkpoint) {
    }

}
