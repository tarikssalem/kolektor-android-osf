/*
 * Copyright (C) 2017 Tárik S. Salem.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.concurrent;

import android.os.Handler;
import android.os.HandlerThread;

public class LooperThread extends HandlerThread {

    private Handler mHandler;

    public LooperThread(String name) {
        super(name);
    }

    public LooperThread(String name, int priority) {
        super(name, priority);
    }

    /**
     * Important to call it before sending anything to the thread.
     */
    @Override
    public synchronized void start() {
        super.start();
        mHandler = new Handler(getLooper());
    }

    /**
     * @see Handler#post(Runnable)
     */
    public void post(Runnable r){
        mHandler.post(r);
    }

    public Handler getHandler() {
        return mHandler;
    }

    @Override
    public boolean quit() {
        mHandler = null;
        return super.quit();
    }

}
