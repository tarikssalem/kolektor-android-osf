/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.action;

import android.util.Log;

import io.kolektor.android.osf.OsfService;
import io.kolektor.android.osf.config.ConfigUpdater;
import io.kolektor.android.osf.config.Configurable;

import static io.kolektor.android.osf.util.LogUtil.TAG;

public class UpdateAction extends Action {

    @Configurable
    protected ConfigUpdater update = null;

    @Configurable
    private String name = null;

    @Configurable
    private OsfService osfService = null;

    UpdateAction() {
    }

    public UpdateAction(String name, OsfService osfService, ConfigUpdater update) {
        this.name = name;
        this.osfService = osfService;
        this.update = update;
    }

    protected void execute() {
        if (name != null && osfService != null && update != null) {
            Log.d(TAG, "UpdateAction: Running config update...");
            update.run(name, osfService);
        } else {
            Log.d(TAG, "UpdateAction: Config update failed.");
        }
    }

    protected boolean isLongRunningAction() {
        return true;
    }

}
