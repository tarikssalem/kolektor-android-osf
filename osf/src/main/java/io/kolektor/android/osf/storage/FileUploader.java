/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.storage;

import android.content.Context;
import android.os.PowerManager.WakeLock;
import android.util.Log;

import java.io.File;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import io.kolektor.android.osf.concurrent.LooperThread;
import io.kolektor.android.osf.config.Configurable;
import io.kolektor.android.osf.time.TimeUtil;
import io.kolektor.android.osf.util.LockUtil;

import static io.kolektor.android.osf.util.LogUtil.TAG;

/**
 * This class is responsible for the upload logic including the retry strategy in the case
 * of a failure. The communication with the remote archive is handled
 * by a {@link RemoteFileArchive} instance.
 * <p>
 * In the case of an unsuccessful file upload, a retry is performed after a timeout. The timeout is
 * individually assigned to each file, and it is being increased the longer a file
 * was not uploaded. See {@link RetryStrategy} for details.
 */
public class FileUploader {

    @Configurable
    private int maxRemoteRetries = 6;

    @Configurable
    private int maxFileRetries = 3;

    private Context mContext;
    private WakeLock mWakeLock;
    private LooperThread mThread;
    private Runnable mWakeLockRelease = new Runnable() {
        @Override
        public void run() {
            releaseWakeLock();
        }
    };
    private Set<File> mFilesToUpload;
    private RetryStrategy mRetryStrategy;

    public FileUploader() {
    }

    public FileUploader(Context context) {
        setContext(context);
    }

    public void start() {
        mThread = new LooperThread(getClass().getName());
        mThread.start();
        mFilesToUpload = Collections.synchronizedSet(new HashSet<File>());
        mRetryStrategy = new RetryStrategy();
    }

    public void quit() {
        mThread.quit();
        releaseWakeLock();
    }

    public void run(final FileArchive localArchive, final RemoteFileArchive remoteArchive) {
        Log.d(TAG, "FileUploader: Running upload...");
        archive(localArchive, remoteArchive);
    }

    private void archive(FileArchive localArchive, RemoteFileArchive remoteArchive) {
        if (localArchive != null && remoteArchive != null) {
            acquireWakeLock();
            for (final File file : localArchive.getAll()) {
                archive(localArchive, remoteArchive, file);
            }
        }
    }

    private void archive(final FileArchive localArchive, final RemoteFileArchive remoteArchive,
            final File file) {
        if (!mFilesToUpload.contains(file) && mRetryStrategy.shouldContinue(remoteArchive, file)) {
            mFilesToUpload.add(file);
            mThread.post(new Runnable() {
                @Override
                public void run() {
                    runArchive(localArchive, remoteArchive, file);
                }
            });
            mThread.getHandler().removeCallbacks(mWakeLockRelease);
            mThread.post(mWakeLockRelease);
        }
    }

    protected void runArchive(FileArchive localArchive, RemoteFileArchive remoteArchive,
            File file) {
        if (remoteArchive.isAvailable()) {
            Log.d(TAG, "FileUploader: Uploading file '" + file.getName() + "'...");
            if (remoteArchive.add(file)) {
                mRetryStrategy.reportSuccess(file);
                mFilesToUpload.remove(file);
                localArchive.remove(file);
                Log.d(TAG, "FileUploader: Uploaded file '" + file.getName() + "'.");
            } else {
                Log.d(TAG, "FileUploader: Failed to upload file '" + file.getName() + "'.");
                mRetryStrategy.reportFailure(file);
                mFilesToUpload.remove(file);
                archive(localArchive, remoteArchive, file);
            }
        } else {
            Log.d(TAG, "FileUploader: Remote archive '" + remoteArchive.getId() + "' not available.");
        }
    }

    private void acquireWakeLock() {
        if (mWakeLock == null) {
            mWakeLock = LockUtil.getWakeLock(mContext);
        }
    }

    private void releaseWakeLock() {
        if (mWakeLock != null && mWakeLock.isHeld()) {
            mWakeLock.release();
        }
        mWakeLock = null;
    }

    public void setContext(Context context) {
        this.mContext = context;
    }

    // ---------------------------------------------------------------------------------------------

    private class RetryStrategy {

        private final static int DEFAULT_RETRY_LIMIT = 2;
        private final static double DEFAULT_RETRY_TIMEOUT = 3600;
        private final static double DEFAULT_RETRY_ALFA = 0.0015;

        private int mRetryLimit = DEFAULT_RETRY_LIMIT;
        private double mRetryTimeout = DEFAULT_RETRY_TIMEOUT;
        private double mRetryAlfa = DEFAULT_RETRY_ALFA;

        private Map<String, Long> mFailureTimestamps;
        private Map<String, Long> mRetryTimestamps;
        private Map<String, Long> mRetryCounts;

        public RetryStrategy() {
            mFailureTimestamps = new ConcurrentHashMap<>();
            mRetryTimestamps = new ConcurrentHashMap<>();
            mRetryCounts = new ConcurrentHashMap<>();
        }

        public synchronized void reportSuccess(File file) {
            String filename = file.getName();
            mFailureTimestamps.remove(filename);
            mRetryTimestamps.remove(filename);
            mRetryCounts.remove(filename);
        }

        public synchronized void reportFailure(File file) {
            String filename = file.getName();
            long retryCount = convertNullToZero(mRetryCounts.get(file.getName())) + 1;
            Log.d(TAG, "FileUploaderRetryStrategy: retryCount = " + retryCount + "; file = '" +
                    filename + "'");
            if (retryCount <= mRetryLimit) {
                mRetryCounts.put(filename, retryCount);
            } else {
                long now = TimeUtil.getTimestamp().longValue();
                Long failureTimestamp = mFailureTimestamps.get(file.getName());
                if (failureTimestamp == null) {
                    failureTimestamp = TimeUtil.getTimestamp().longValue();
                    mFailureTimestamps.put(filename, failureTimestamp);
                }
                long failureDuration = now - failureTimestamp;
                long retryTimestamp = now + calcRetryTimeout(failureDuration);
                mRetryTimestamps.put(filename, retryTimestamp);
                mRetryCounts.remove(filename);
                Log.d(TAG, "FileUploaderRetryStrategy: retryTimestamp = " + new Date(retryTimestamp * 1000) +
                        "; file = '" + filename + "'");
            }
        }

        public synchronized boolean shouldContinue(RemoteFileArchive remmoteArchive, File file) {
            long now = TimeUtil.getTimestamp().longValue();
            long retryTimestamp = convertNullToZero(mRetryTimestamps.get(file.getName()));
            long retryCount = convertNullToZero(mRetryCounts.get(file.getName()));
            if (retryTimestamp < now && retryCount <= mRetryLimit) {
                return true;
            }
            return false;
        }

        private long convertNullToZero(Long l) {
            return l != null ? l : 0;
        }

        /**
         * Timeout function:
         *
         * f(t) = T * [1 - e^(-α*t)]
         *
         * t ... time passed since first failure
         * T ... maximum timeout
         * α ... defines the growth of timeout
         * f(t) ... timeout until next try
         *
         * Calculating α:
         *
         * α = - ln(1 - β) / t_β
         *
         * @param failureDuration elapsed time (in seconds) since the first failure
         * @return timeout in seconds
         */
        private long calcRetryTimeout(double failureDuration) {
            return Math.round(DEFAULT_RETRY_TIMEOUT *
                    (1 - Math.exp(-DEFAULT_RETRY_ALFA * failureDuration)));
        }

    }

}
