/*
 * Copyright (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.storage;

import android.content.Context;

import java.io.File;

import io.kolektor.android.osf.config.Configurable;
import io.kolektor.android.osf.util.FileUtil;
import io.kolektor.android.osf.util.StringUtil;

public abstract class AbstractArchive implements FileArchive {

    protected static final String ARCHIVE_DIR_PREFIX = "osf_";
    protected static final String ARCHIVE_DIR_SUFFIX = "_archive";
    protected static final String BACKUP_DIR_SUFFIX = "_backup";

    @Configurable
    protected String name = "default";

    protected Context context;

    /**
     * Returns the delegate archive.
     *
     * @return the delegate archive
     */
    protected abstract FileArchive getDelegateArchive();

    protected File getInternalArchiveDir() {
        return context.getDir(getArchiveDirName(), Context.MODE_PRIVATE);
    }

    protected File getExternalArchiveDir() {
        return new File(buildExternalStoragePath(getArchiveDirName()));
    }

    protected File getBackupArchiveDir() {
        return new File(buildExternalStoragePath(getBackupDirName()));
    }

    protected String buildExternalStoragePath(String dirName) {
        return FileUtil.getExternalStoragePath(context) + dirName + "/";
    }

    protected String getArchiveDirName() {
        return ARCHIVE_DIR_PREFIX + getSanitizedName() + ARCHIVE_DIR_SUFFIX;
    }

    protected String getBackupDirName() {
        return ARCHIVE_DIR_PREFIX + getSanitizedName() + BACKUP_DIR_SUFFIX;
    }

    protected String getSanitizedName() {
        return StringUtil.sanitize(name);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean add(File item) {
        return getDelegateArchive().add(item);
    }

    @Override
    public boolean contains(File item) {
        return getDelegateArchive().contains(item);
    }

    @Override
    public File[] getAll() {
        return getDelegateArchive().getAll();
    }

    @Override
    public boolean remove(File item) {
        return getDelegateArchive().remove(item);
    }

}
