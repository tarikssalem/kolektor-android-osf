/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.storage;

import java.io.File;

/**
 * Interface for representing file archives that are not on the device.
 */
public interface RemoteFileArchive {

    /**
     * Synchronously add the file to the remote archive.
     *
     * @param file the file to upload
     * @return true if successfully added, false otherwise
     */
    public boolean add(File file);

    /**
     * @return true if this remote archive is available currently
     */
    public boolean isAvailable();

    /**
     * A unique string that represents this remote archive. These will mostly be URIs,
     * but implementation is dependent on implementation.
     *
     * @return
     */
    public String getId();

}
