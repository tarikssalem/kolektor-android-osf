/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.pipeline;

import com.google.gson.JsonElement;

import io.kolektor.android.osf.OsfService;

public interface Pipeline {
	
	/**
	 * Called once when the pipeline is created. This method can be used
	 * to register any scheduled operations.
	 * 
	 * @param osfService
	 */
	public void onCreate(OsfService osfService);
	
	/**
	 * Instructs pipeline to perform an operation.
	 * @param action The action to perform.
	 * @param config The object to perform the action on.
	 */
	public void onRun(String action, JsonElement config); // maybe intent, IJsonObject?
	
	/**
	 * The teardown method called once when the pipeline should shut down.
	 */
	public void onDestroy();
	
	/**
	 * Returns true if this pipeline is enabled, meaning onCreate has been called 
	 * and onDestroy has not yet been called.
	 */
	public boolean isEnabled();
	
}
