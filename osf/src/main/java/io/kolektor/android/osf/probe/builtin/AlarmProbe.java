/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.probe.builtin;

import android.util.Log;

import com.google.gson.JsonObject;

import io.kolektor.android.osf.OsfService;
import io.kolektor.android.osf.Schedule;
import io.kolektor.android.osf.config.Configurable;
import io.kolektor.android.osf.json.JsonUtils;
import io.kolektor.android.osf.probe.Probe;
import io.kolektor.android.osf.time.TimeUtil;

import static io.kolektor.android.osf.util.LogUtil.TAG;

/**
 * There are three kinds of schedules possible with this AlarmProbe:
 * 
 *     1. interval > 0, offset >= 0:
 *          This schedules the alarm to go off at every "interval" seconds,
 *          starting from the Unix timestamp referred to by "offset" (in seconds).
 *          If "offset" is in the past, the start time is set as the immediate next
 *          time in the future that occurs in the sequence starting at "offset"
 *          and having a period of "interval" seconds.
 *     2. interval > 0, offset = null or < 0:
 *          This schedules the alarm to go off at every "interval" seconds,
 *          starting from the instant when this function is executed.
 *     3. interval = null or <= 0, offset >= 0:
 *          This schedules a one-time alarm to go off at the Unix timestamp
 *          referred by "offset" (in seconds).
 * 
 * For types 1 and 2, the flag "exact" determines whether the alarm will go off exactly
 * or approximately at the specified times (inexact alarms use less battery power)
 * For type 3, the value of "exact" is immaterial; the alarm will go off exactly at
 * the specified time.
 */
@Probe.DisplayName("Alarm Probe")
@Schedule.DefaultSchedule(interval=Probe.DEFAULT_PERIOD, duration= Probe.ContinuousProbe.DEFAULT_DURATION)
public class AlarmProbe extends Probe.Base implements Probe.ContinuousProbe, Runnable {

    /**
     * In seconds. Only positive values considered as valid.
     */
    @Configurable
    private Long interval = null;

    @Configurable
    private boolean exact = false;

    /**
     * In seconds. Only non-negative values considered as valid.
     */
    @Configurable
    private Long offset = null;

    @Override
    public void run() {
        sendData(new JsonObject());
        Log.d(TAG, "AlarmProbe: Triggered — " + getConfig());
    }

    protected void onStart() {
        String probeConfig = getConfig().toString();
        long intervalMillis = (interval == null || interval < 0) ? 0 : TimeUtil.secondsToMillis(interval);
        long offsetMillis = (offset == null || offset < 0) ? -1 : TimeUtil.secondsToMillis(offset);
        long currentMillis = System.currentTimeMillis();
        if (intervalMillis > 0) {
            if (offsetMillis >= 0) {  // Type 1
                if (offsetMillis <= currentMillis) { // Offset is in the past.
                    long intervalDeltaMillis = (currentMillis - offsetMillis) % intervalMillis;
                    long timeToNextAlarmMillis = intervalMillis - intervalDeltaMillis;
                    offsetMillis = currentMillis + timeToNextAlarmMillis;
                }
                OsfService.registerAlarm(getContext(), probeConfig, offsetMillis, intervalMillis, exact);
            } else {  // Type 2
                OsfService.registerAlarm(getContext(), probeConfig, currentMillis, intervalMillis, exact);
            }   
        } else {  // Type 3
            assert (offsetMillis >= 0); // Offset must be valid.
            // If offset is in the past, alarm will fire immediately.
            OsfService.registerAlarm(getContext(), probeConfig, offsetMillis, intervalMillis, exact);
        }
        Log.d(TAG, "AlarmProbe: Registered — " + getConfig());
    }

    protected void onStop() {
        String probeConfig = JsonUtils.immutable(getGson().toJsonTree(this)).toString();
        OsfService.unregisterAlarm(getContext(), probeConfig);
        Log.d(TAG, "AlarmProbe: Unregistered — " + getConfig());
    }

    @Override
    protected boolean isWakeLockedWhileRunning() {
        return false;
    }

}
