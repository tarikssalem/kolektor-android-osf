/*
 * Portions (C) 2017 Tárik S. Salem.
 * Copyright (C) 2010-2011 Nadav Aharony, Wei Pan, Alex Pentland. ACK: Alan Gardner.
 *
 * This file is part of Kolektor Open Sensing Framework aka Kolektor OSF.
 *
 * Kolektor OSF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kolektor OSF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kolektor OSF. If not, see <http://www.gnu.org/licenses/>.
 */
package io.kolektor.android.osf.action;

import io.kolektor.android.osf.config.Configurable;
import io.kolektor.android.osf.storage.DatabaseHelper;
import io.kolektor.android.osf.storage.FileArchive;

import android.util.Log;

import static io.kolektor.android.osf.util.LogUtil.TAG;

public class ArchiveAction extends Action {

    @Configurable
    private FileArchive archive = null;

    @Configurable
    private DatabaseHelper dbHelper = null;

    ArchiveAction() {
    }

    public ArchiveAction(FileArchive archive, DatabaseHelper dbHelper) {
        this.archive = archive;
        this.dbHelper = dbHelper;
    }

    @Override
    protected void execute() {
        if (dbHelper.isEmpty()) {
            Log.d(TAG, "ArchiveAction: Nothing to archive.");
            return;
        }

        Log.d(TAG, "ArchiveAction: Archiving...");
        synchronized (dbHelper) {
            dbHelper.close();
            if (archive.add(dbHelper.getFile())) {
                dbHelper.delete();
            }
            dbHelper.initialize(); // Build new database
        }
        Log.d(TAG, "ArchiveAction: Archived.");
    }

    protected boolean isLongRunningAction() {
        return true;
    }

}
